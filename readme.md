## KeepCloset

- Nada de mais, ateh agora... rs
- qdo o phpsc/pagseguro der um problema de annotation: `
require indigophp/doctrine-annotation-autoload
`

- public\resources\medias _(feito talvez automático)_
```
mkdir storage
mkdir storage/app
mkdir storage/app/attachments
mkdir storage/framework
mkdir storage/framework/sessions
mkdir storage/framework/views
```
- linux _(public folder)_:
```
// linux
ln -s ../storage/app storage
```
- windows _(public folder && run as admin)_
```
// windows
mklink /D storage ..\storage\app
```

- Problemas de memory_limit running composer no linux
```
php -d memory_limit=2048M ~/bin/composer/composer.phar update -vvv
```

- and done!