<?php


Route::get('/', 'WelcomeController@index')->name('home');
Route::post('/ebook', 'WelcomeController@postEbook')->name('ebook');
Route::post('/send-email', 'WelcomeController@sendEmail')
    ->name('welcome.send-email');
Route::post('/contact', 'WelcomeController@contact')->name('welcome.contact');
Route::get('blog', 'WelcomeController@blog')->name('welcome.blog');
Route::get('tutorials', 'WelcomeController@tutorials')->name('welcome.tutorials');


// mobile
Route::group(['prefix' => 'mobile'], function() {

    Route::get('support', 'MobileSupportController@index');
    Route::get('support/welcome', 'MobileSupportController@welcome')
        ->name('mobile.support.welcome');
    Route::get('support/blog', 'MobileSupportController@blog')
        ->name('mobile.support.blog');
    Route::get('support/tutorials', 'MobileSupportController@tutorials')
        ->name('mobile.support.tutorials');
    Route::get('support/qm-usa', 'MobileSupportController@qmUsa')
        ->name('mobile.support.qmUsa');
    Route::get('support/faq', 'MobileSupportController@faq')
        ->name('mobile.support.faq');
    Route::get('support/qm-somos-nos', 'MobileSupportController@qmSomosNos')
        ->name('mobile.support.qmSomosNos');


    //
    Route::get('generics', 'MobileGenericController@index');

});

// admin
Route::group(['prefix' => 'admin'], function() {

    Route::get('/', 'AdminController@index');

    // generics
    Route::get('json/generics', 'AdminJsonController@getGenerics');
    Route::get('json/generic/items', 'AdminJsonController@getGenericItems');
    Route::post('json/generics', 'AdminJsonController@saveItem');
    Route::delete('json/generic/{itemId}/destroy', 'AdminJsonController@destroyItem');
    Route::post('json/generic/positions', 'AdminJsonController@postPositions');

    // blog publisher
    Route::get('json/blog/publisher', 'AdminJsonBlogPublisherController@index');
    Route::post('json/blog/publisher', 'AdminJsonBlogPublisherController@save');
    Route::delete('json/blog/publisher/{blogPublisherId}', 'AdminJsonBlogPublisherController@destroy');

    // blog
    Route::get('json/blog', 'AdminJsonBlogController@index');
    Route::post('json/blog', 'AdminJsonBlogController@save');
    Route::delete('json/blog/{blogId}', 'AdminJsonBlogController@destroy');

    // attachment
    Route::post('json/attachment/create', 'AdminJsonAttachmentController@create');
    Route::delete('json/attachment/{attachmentId}', 'AdminJsonAttachmentController@destroy');

});




// test
Route::get('test', function() {

    return [44 => 'dino'];

//    return \App\Blog::get(['id'])->pluck('id');

});

