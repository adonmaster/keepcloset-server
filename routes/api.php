<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register/code', 'ApiRegisterController@code');
Route::post('register/check', 'ApiRegisterController@check');



Route::get('test', function() {
    return 2243;
});