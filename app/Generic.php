<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Generic
 *
 * @package App
 * @property int $id
 * @property string $name
 * @property string $desc
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GenericItem[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Generic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Generic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Generic query()
 */
class Generic extends Model
{
    protected $table = 'generics';
    protected $fillable = [
        'name', 'desc'
    ];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = [];

    # Relationships

    public function items()
    {
        return $this->hasMany(GenericItem::class, 'generic_id', 'id')
            ->orderBy('position')
            ->orderBy('id');
    }
}
