<?php namespace App;
use App\Adon\Presenters\Engine\PresenterTrait;
use App\Adon\Presenters\GenericItemPresenter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class GenericItem
 *
 * @package App
 * @property int $id
 * @property int $generic_id
 * @property string $body
 * @property int $position
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GenericItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GenericItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GenericItem query()
 * @property-read \App\Attachment $attachment
 */
class GenericItem extends Model
{
    protected $table = 'generic_item';
    protected $fillable = ['generic_id', 'position', 'body'];
    protected $dates = [];
    public $timestamps = true;
    protected $casts = ['body' => 'array', 'position' => 'int'];


    // presenter

    use PresenterTrait;

    /**
     * @return GenericItemPresenter
     */
    public function p()
    {
        return $this->present();
    }


    # Relationships

    public function attachment()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

}