<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Registration
 *
 * @package App
 * @property int $id
 * @property string $email
 * @property string $code
 * @property Carbon $expired_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Registration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Registration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Registration query()
 * @mixin \Eloquent
 */
class Registration extends Model
{
    protected $table = 'registrations';
    protected $hidden = ['code', 'expired_at'];
    protected $fillable = [
        'email', 'code', 'expired_at'
    ];
    protected $dates = ['expired_at'];
    public $timestamps = false;
    protected $casts = [];
    protected $with = [];

    /**
     * @param string $email
     * @param string $code
     * @param Carbon $expired_at
     * @return Registration
     */
    public static function init($email, $code, $expired_at) {
        return new self(compact('email', 'code', 'expired_at'));
    }

}
