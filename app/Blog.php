<?php

namespace App;

use App\Adon\Presenters\BlogPresenter;
use App\Adon\Presenters\Engine\PresenterInterface;
use App\Adon\Presenters\Engine\PresenterTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Blog
 *
 * @package App
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $publisher_id
 * @property Carbon $published_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attachment[] $attachments
 * @property-read \App\BlogPublisher $publisher
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Blog query()
 * @mixin \Eloquent
 */
class Blog extends Model implements PresenterInterface
{
    protected $table = 'blog';
    protected $fillable = ['title', 'body', 'publisher_id', 'published_at'];
    protected $dates = ['published_at'];
    public $timestamps = true;
    protected $casts = [];
    protected $with = ['publisher'];

    # presenter
    function presenter() {
        return new BlogPresenter($this);
    }

    use PresenterTrait;

    /**
     * @return BlogPresenter
     */
    public function p() {
        return $this->present();
    }


    # Relationship

    public function publisher()
    {
        return $this->belongsTo(BlogPublisher::class, 'publisher_id', 'id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

}
