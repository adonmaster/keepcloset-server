<?php namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * PLACEHOLDER!
 * 
 * Class AttachableModel
 *
 * @package App
 * @property-read \App\Attachment $attachment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attachment[] $attachments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttachableModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttachableModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttachableModel query()
 * @mixin \Eloquent
 */
class AttachableModel extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function attachment()
    {
        return $this->morphOne(Attachment::class, 'attachable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

}