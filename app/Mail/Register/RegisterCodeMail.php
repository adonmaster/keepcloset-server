<?php

namespace App\Mail\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $code;

    /**
     * @var
     */
    private $email;

    /**
     * Create a new message instance.
     *
     * @param $email
     * @param $code
     */
    public function __construct($email, $code)
    {
        $this->code = $code;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->email)
            ->subject('Confirmação de registro')
            ->view('mail.register.code');
    }
}
