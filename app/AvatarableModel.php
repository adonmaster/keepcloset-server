<?php namespace App;


use Illuminate\Database\Eloquent\Relations\MorphOne;

interface AvatarableModel
{
    /**
     * @return MorphOne
     */
    public function avatar();
}