<?php

namespace App\Exceptions;

use App\Adon\Flash\Flash;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $route = (\Auth::check()) ? 'client.index' : 'client.login';
        $msg = $exception->getMessage();

        if ($request->expectsJson())
        {
            $pack = ['message' => starts_with($msg, 'Unauthen') ? 'Não autorizado.' : $msg];
            return response()->json($pack, 401);
        }
        else
        {
            Flash::error('Não autorizado!');

            return redirect()->guest(route($route));
        }
    }
}
