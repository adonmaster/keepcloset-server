<?php namespace App\Exceptions;


class RepoException extends \Exception
{

    /**
     * RepoException constructor.
     */
    public function __construct($message)
    {
        parent::__construct($message, 400);
    }
}