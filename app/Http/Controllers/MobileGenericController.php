<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class MobileGenericController extends Controller
{

    public function index()
    {
        $data = request()->validate([
            'parent' => 'required'
        ]);

        return Repo::generic()->getItemsFrom($data['parent']);
    }

}
