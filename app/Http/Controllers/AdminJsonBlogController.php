<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use App\Adon\Sanitizer\BrSanitizer;
use App\Adon\Sanitizer\Sanitizer;
use App\Blog;
use Illuminate\Http\Request;

class AdminJsonBlogController extends Controller
{

    /**
     * AdminJsonBlogController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.basic.admin');
    }

    public function index()
    {
        return Repo::blog()->getWithRel();
    }

    public function save()
    {
        $data = request()->validate([
            'id' => 'required',
            'title' => 'required',
            'body' => 'required',
            'published_at' => 'required|date_format:d/m/Y',
            'publisher_id' => 'required',
            'attachments' => 'array'
        ]);

        $data = Sanitizer::run($data, [
            'published_at' => BrSanitizer::$date
        ]);

        /**
         * @var int $id
         * @var $title
         * @var $body
         * @var $published_at
         * @var $publisher_id
         * @var int[] $attachments
         */
        extract($data);

        $blog = Repo::blog()->save($id, $title, $body, $published_at, $publisher_id);

        Repo::attachment()->syncIds($attachments, Blog::class, $blog->id);

        return Repo::blog()->findWithRel($blog->id);
    }

    public function destroy($blogId)
    {
        Repo::blog()->destroy($blogId);

        return $this->jsonResponseOk();
    }
}
