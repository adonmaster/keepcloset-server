<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\RequiredIf;

class AdminJsonAttachmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic.admin');
    }

    public function create()
    {
        $data = request()->validate([
            'attachable_type' => 'required',
            'attachable_id' => 'required',
            'type' => 'required',
            'file' => Rule::requiredIf(request('type')=='image'),
            'url' => [Rule::requiredIf(request('type')=='youtube'), 'regex:/^http(?:s)?:\/\/www.youtube.com\/embed\/(.+)?.+$/']
        ], [
            'regex' => 'É necessário que seja um link do youtube válido. E deve ser [incorporado] também.'
        ]);

        /**
         * @var $attachable_type
         * @var $attachable_id
         * @var $type
         */
        extract($data);

        if ($type=='image') {
            return Repo::attachment()
                ->create($attachable_type, $attachable_id, $type, request()->file('file')[0]);
        }

        else if ($type=='youtube') {
            return Repo::attachment()
                ->createWithUrl($attachable_type, $attachable_id, $type, $data['url']);
        }

        return 0;
    }

    public function destroy($attachmentId)
    {
        Repo::attachment()->remove($attachmentId);

        return $this->jsonResponseOk();
    }
}
