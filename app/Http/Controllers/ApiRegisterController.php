<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use App\Mail\Register\RegisterCodeMail;
use Illuminate\Http\Request;

class ApiRegisterController extends Controller
{
    public function code()
    {
        $data = request()->validate([
            'email' => 'required|email'
        ]);

        $model = Repo::registration()->create($data['email']);

        \Mail::send(new RegisterCodeMail($model->email, $model->code));

        return $this->jsonResponsePack($model);
    }

    public function check()
    {
        $data = request()->validate([
            'email' => 'required',
            'code' => 'required'
        ]);

        try {
            Repo::registration()->validate($data['email'], $data['code']);
            return $this->jsonResponseOk();
        } catch (\Exception $exception) {
            return $this->jsonResponseError($exception->getMessage());
        }
    }
}
