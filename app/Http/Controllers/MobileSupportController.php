<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class MobileSupportController extends Controller
{
    public function index()
    {
        return $this->jsonResponsePack([
            [
                'title' => 'SEJA BEM VINDO',
                'desc' => 'Mensagem de boas vindas...',
                'url' => route('mobile.support.welcome')
            ],
            [
                'title' => 'TUTORIAIS',
                'desc' => 'Assista os vídeos para aprender...',
                'url' => route('mobile.support.tutorials')
            ],
            [
                'title' => 'QUEM USA',
                'desc' => 'Famosos e não fomosos usam nosso app!',
                'url' => route('mobile.support.qmUsa')
            ],
            [
                'title' => 'LIFESTYLE',
                'desc' => 'Blog da nossa ferramenta...',
                'url' => route('mobile.support.blog')
            ],
            [
                'title' => 'PERGUNTAS FREQUENTES',
                'desc' => 'Faq...',
                'url' => route('mobile.support.faq')
            ],
            [
                'title' => 'QUEM SOMOS',
                'desc' => 'Olha só quem faz tudo isso acontecer...',
                'url' => route('mobile.support.qmSomosNos')
            ],
        ]);
    }

    public function welcome()
    {
        return view('mobile.support-welcome');
    }

    public function blog()
    {
        $blog = Repo::blog()->paginateWithRel();

        return view('mobile.support-blog', compact('blog'));
    }

    public function tutorials()
    {
        return view('mobile.support-tutorials');
    }

    public function qmUsa()
    {
        return view('mobile.support-qm-usa');
    }

    public function faq()
    {
        return view('mobile.support-faq');
    }

    public function qmSomosNos()
    {
        return view('mobile.support-qm-somos-nos');
    }
}
