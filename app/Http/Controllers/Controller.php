<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return null|User
     */
    protected function user()
    {
        return \Auth::user();
    }

    /**
     * @return int
     */
    protected function uid() {
        return \Auth::id();
    }

    /**
     * @param string $msg
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponseOk($msg='Maravilha!', $code=200)
    {
        return $this->jsonResponse($msg, $code);
    }

    protected function jsonResponsePack($pack, $msg='Maravilha', $code=200) {
        return $this->jsonResponse($msg, $code, $pack);
    }

    /**
     * @param string $msg
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponseError($msg, $code=400) {
        return $this->jsonResponse($msg, $code);
    }

    /**
     * @param string $message
     * @param int $code
     * @param array $pack
     * @return \Illuminate\Http\JsonResponse
     */
    private function jsonResponse($message, $code=400, $pack=[]) {
        return response()->json(
            compact('message', 'code', 'pack'), $code
        );
    }
}
