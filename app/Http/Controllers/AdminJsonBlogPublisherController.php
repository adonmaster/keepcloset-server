<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminJsonBlogPublisherController extends Controller
{

    /**
     * AdminJsonBlogPublisherController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.basic.admin');
    }


    public function index()
    {
        return Repo::blogPublisher()->getAll();
    }

    public function save()
    {
        $data = request()->validate([
            'id' => 'required',
            'name' => 'required',
            'insta' => 'required',
            'avatar_img' => Rule::requiredIf(request('id')==0)
        ]);

        /**
         * @var $id
         * @var $name
         * @var $insta
         */
        extract($data);

        $model = Repo::blogPublisher()->save($id, $name, $insta);

        // save avatar
        if (request()->hasFile('avatar_img')) {
            Repo::avatar()->saveTo($model, request()->file('avatar_img')[0]);
        }

        return Repo::blogPublisher()->findWithRel($model->id);
    }

    public function destroy($blogPublisherId)
    {
        // remove avatar
        if ($model = Repo::blogPublisher()->findWithRel($blogPublisherId))
        {
            if ($count = Repo::blogPublisher()->countBlogFor($model->id))
            {
                $msg = "Este publicador tem $count post(s) anexados a ele. Apague ou edit os posts relacionados!";

                return $this->jsonResponseError($msg);
            }

            Repo::avatar()->removeFrom($model);
            Repo::blogPublisher()->destroy($blogPublisherId);
        }

        return $this->jsonResponseOk();
    }
}
