<?php

namespace App\Http\Controllers;

use App\Adon\Flash\Flash;
use App\Adon\Repo\Repo;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function postEbook()
    {

        $email = request()->validate(['email' => 'required|email'])['email'];
        $ebookPath = storage_path('app/ebook.pdf');

        // send email
        \Mail::send('mail.ebook', [], function($m) use($email, $ebookPath) {
            /**
             * @var Mailable $m
             */
            $m
                ->to($email)
                ->subject('KeepCloset Ebook!')
                ->attach($ebookPath);
        });

        // persist
        Repo::generic()->add('ebook.emails', compact('email'), 'Ebook Mailing List');

        // flash
        Flash::success('Parabéns! Você acabou de receber em seu email nosso ebook.');

        // bbye
        return redirect()->route('home');
    }

    public function sendEmail()
    {
        $data = request()->validate([
            'email' => 'email'
        ]);

        Repo::generic()->add('newsletter', $data, 'Newsletter');

        Flash::success('Email armazenado com sucesso.<br>Aguarde nossas novidades!');

        return redirect()->route('home');
    }

    public function contact()
    {
        $data = request()->all();

        $validator = \Validator::make($data, [
            'email' => 'required|email',
            'subject' => 'required',
            'body' => 'required',
        ]);

        // validator fails
        if ($validator->fails()) {

            return redirect('/#container-contact')
                ->withErrors($validator)
                ->withInput();
        }

        // send email
        \Mail::send('mail.contact-to-admin', $data, function($m) {
            /**
             * @var Mailable $m
             */
            $m
                ->to(getenv('ADMIN_EMAIL'))
                ->subject('KeepCloset Contato enviado!');
        });

        Flash::success('Informações enviadas com sucesso! Obrigado');

        return redirect('/#container-contact');
    }

    public function blog()
    {
        $blog = Repo::blog()->paginateWithRel();

        return view('blog', compact('blog'));
    }

    public function tutorials()
    {
        return view('tutorials');
    }
}
