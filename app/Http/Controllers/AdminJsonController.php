<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use App\GenericItem;
use Illuminate\Http\Request;

class AdminJsonController extends Controller
{

    /**
     * AdminJsonController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth.basic.admin');
    }

    public function getGenerics()
    {
        $parents = request('parents') ?: [];

        return Repo::generic()->getFrom($parents);
    }

    public function getGenericItems()
    {
        $data = request()->validate([
            'parent' => 'required'
        ]);

        return Repo::generic()->getItemsFrom($data['parent']);
    }

    public function saveItem()
    {
        $data = request()->validate([
            'parent_name' => 'required',
            'id' => 'required',
            'body' => 'required',
            'position' => 'required'
        ]);

        $isEditing = $data['id'] > 0;
        extract($data);

        $model = Repo::generic()->saveItem($parent_name, $id, $body, $position);

        // file
        if (request()->hasFile('file')) {
            if ($isEditing) Repo::attachment()->removeFrom($model);

            $file = request()->file('file');
            $file = is_array($file) ? $file[0] : $file;

            Repo::attachment()->saveFor($model, $file);
        }

        // multiple attachments
        $attachments = request('attachments');
        if ($attachments) {
            Repo::attachment()->syncIds($attachments, GenericItem::class, $model->id);
        }

        // load relationships
        $model = Repo::generic()->findItemWithRel($model->id);

        return $model;
    }

    public function destroyItem($itemId)
    {
        Repo::generic()->destroyItem($itemId);

        return $this->jsonResponseOk();
    }

    public function postPositions()
    {
        $positions = request('positions') ?: [];

        Repo::generic()->positionItems($positions);

        return $this->jsonResponseOk();
    }
}
