<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Avatar
 *
 * @package App
 * @property int $id
 * @property string $avatarable_type
 * @property int $avatarable_id
 * @property string $path
 * @property string $url
 * @property string $path_thumb
 * @property string $url_thumb
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $avatarable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avatar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avatar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avatar query()
 * @mixin \Eloquent
 */
class Avatar extends Model
{
    protected $table = 'avatars';
    protected $fillable = ['avatarable_type', 'avatarable_id', 'path', 'url', 'path_thumb', 'url_thumb'];
    protected $hidden = ['path', 'path_thumb'];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = [];
    protected $with = [];


    # Relationships

    public function avatarable()
    {
        return $this->morphTo();
    }
}
