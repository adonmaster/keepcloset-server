<?php namespace App\Adon\Presenters\Engine;


class BasePresenter {

    protected $model;

    /**
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }
}
