<?php namespace App\Adon\Presenters\Engine;


interface PresenterInterface {

    function presenter();

    function present();
}