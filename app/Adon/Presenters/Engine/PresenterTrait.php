<?php namespace App\Adon\Presenters\Engine;


trait PresenterTrait {

    private $presenterInstance;

    function present()
    {
        if ($this->presenterInstance)
        {
            return $this->presenterInstance;
        }

        if ($this instanceof PresenterInterface)
        {
            return $this->presenterInstance = $this->presenter();
        }

        throw new \Exception('Implemente a bosta da PresenterInterface');
    }

}