<?php namespace App\Adon\Presenters;


use App\Adon\Presenters\Engine\BasePresenter;
use App\Attachment;

class BlogPresenter extends BasePresenter
{
    public function body()
    {
        $body = $this->model->body;

        // attachments injection
        $elements = [];
        foreach ($this->model->attachments as $a) {
            $elements[$a->id] = $this->bodyRender($a);
        }

        foreach ($elements as $key => $el) {
            $body = str_replace("[#$key]", $el, $body);
        }

        return nl2br($body);
    }

    /**
     * @param Attachment $a
     * @return string|null
     */
    private function bodyRender($a)
    {
        if ($a->type=='youtube')
        {
            return "
                <div v-if=\"model.type=='youtube'\" class=\"embed-responsive embed-responsive-16by9\">
                    <iframe class=\"embed-responsive-item\" src=\"{$a->url}\" allowfullscreen></iframe>
                </div>
            ";
        }

        else if ($a->type=='image')
        {
            return "
                <img src=\"{$a->url}\" width=\"100%\" style=\"object-fit: cover\">
            ";
        }

        return null;
    }
}