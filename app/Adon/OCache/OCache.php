<?php namespace App\Adon\OCache;


class OCache
{
    /**
     * @var array
     */
    private static $cache = [];

    /**
     * @param string $className
     *
     * @return mixed
     */
    public static function retrieve($className)
    {
        if (isset(self::$cache[$className]))
        {
            return self::$cache[$className];
        }

        return self::$cache[$className] = \App::make($className);
    }

    /**
     * @param string $className
     */
    public static function forget($className)
    {
        if (isset(self::$cache[$className]))
        {
            unset(self::$cache[$className]);
        }
    }
}