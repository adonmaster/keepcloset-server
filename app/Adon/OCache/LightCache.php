<?php namespace App\Adon\OCache;


use Closure;



class LightCache {

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @param string $key
     * @param mixed $data
     * @return mixed
     */
    public function put($key, $data)
    {
        $this->cache[$key] = $data;

        return $data;
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public function get($key, $default=null)
    {
        if (array_key_exists($key, $this->cache)) return $this->cache[$key];

        return $default;
    }

    /**
     * @param string $key
     * @param Closure $cbInstanceMaker
     * @return mixed|null
     */
    public function remember($key, Closure $cbInstanceMaker)
    {
        if ($val = $this->get($key)) return $val;

        return $this->put($key, $cbInstanceMaker());
    }

    /**
     * @param string $key
     */
    public function forget($key)
    {
        unset($this->cache[$key]);
    }

}