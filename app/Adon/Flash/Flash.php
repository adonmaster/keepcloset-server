<?php namespace App\Adon\Flash;


class Flash
{
    public static function error($body, $title='Eita!') {
        self::alert('alert-danger', $title, $body);
    }

    public static function info($body, $title='Oba!') {
        self::alert('alert-info', $title, $body);
    }

    public static function success($body, $title='Maravilha!') {
        self::alert('alert-success', $title, $body);
    }

    public static function warning($body, $title = 'Nossa!')
    {
        self::alert('alert-warning', $title, $body);
    }

    public static function has() {
        return (\Session::has('adon-flash-type'));
    }

    public static function render()
    {
        if (self::has())
        {
            $type = \Session::get('adon-flash-type');
            $title = \Session::get('adon-flash-title');
            $body = \Session::get('adon-flash-body');

            $icon = '';
            switch ($type) {
                case 'alert-info':
                    $icon = '<i class="fa fa-info-circle fa-4x"></i><br>';
                    break;

                case 'alert-danger':
                    $icon = '<i class="fa fa-close fa-4x"></i><br>';
                    break;

                case 'alert-warning':
                    $icon = '<i class="fa fa-exclamation-triangle fa-4x"></i><br>';
                    break;

                case 'alert-success':
                    $icon = '<i class="fa fa-check fa-4x"></i><br>';
                    break;
            }


            return "
                <div class=\"alert $type alert-dismissible\" role=\"alert\">
                <!--
                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                  -->
                  <div class='text-center'>
                    $icon
                    <strong>$title</strong>
                    <br>$body
                  </div>
                </div>
                ";
        }
        return '';
    }

    private static function alert($type, $title, $body)
    {
        \Session::flash('adon-flash-type', $type);
        \Session::flash('adon-flash-title', $title);
        \Session::flash('adon-flash-body', $body);
    }
}