<?php namespace App\Adon;


class Ver {

    static public $NAME = "0.0.2";

    static public function asset($path) {
        $ver = urlencode(self::$NAME);
        return asset("$path?$ver", getenv('APP_SSL')=='true');
    }

    static public function assetSecure($path) {
        return asset($path, getenv('APP_SSL')=='true');
    }

}