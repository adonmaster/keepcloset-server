<?php namespace App\Adon\Middlewares;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SecureMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return Closure|Response
     */
    public function handle($request, Closure $next)
    {
        if (getenv('APP_SSL')=='true' && !$request->isSecure())
        {
            return redirect()->secure($request->path());
        }

        return $next($request);
    }
}