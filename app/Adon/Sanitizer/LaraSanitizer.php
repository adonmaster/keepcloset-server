<?php namespace App\Adon\Sanitizer;


use Carbon\Carbon;

class LaraSanitizer
{
    public static $date = LaraSanitizer::class . '@date';
    public static $dateTime = LaraSanitizer::class . '@dateTime';
    public static $dateTimeArray = LaraSanitizer::class . '@dateTimeArray';

    public function date($v)
    {
        return Carbon::createFromFormat('Y-m-d', $v);
    }

    public function dateTime($v)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $v);
    }

    public function dateTimeArray(array $arr)
    {
        return array_map(function($dateString) {
            return $this->dateTime($dateString);
        }, $arr);
    }
}