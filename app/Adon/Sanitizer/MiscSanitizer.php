<?php namespace App\Adon\Sanitizer;


class MiscSanitizer
{

    public static $grantArray = self::class . '@grantArray';
    public static $grantNull = self::class . '@grantNull';
    public static $grantZero = self::class . '@grantZero';
    public static $toBoolean = self::class . '@toBoolean';
    public static $toDouble = self::class . '@toDouble';


    public function grantArray($v)
    {
        if (!$v) return [];

        if (!is_array($v)) return [$v];

        return $v;
    }

    public function grantNull($v) {
        return $v ?: null;
    }

    public function grantZero($v) {
        return $v ?: 0;
    }

    public function toBoolean($v)
    {
        if (is_null($v)) return null;
        return $v == 'true';
    }

    public function toDouble($v)
    {
        if (is_null($v)) return null;
        return doubleval($v);
    }
}