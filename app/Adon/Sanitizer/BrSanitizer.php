<?php namespace App\Adon\Sanitizer;


use Carbon\Carbon;

class BrSanitizer
{
    public static $date = BrSanitizer::class . '@date';
    public static $dateTime = BrSanitizer::class . '@dateTime';
    public static $dateOrNull = BrSanitizer::class . '@dateOrNull';
    public static $decimal = BrSanitizer::class . '@decimal';

    public function date($v)
    {
        return Carbon::createFromFormat('d/m/Y', $v);
    }

    public function dateTime($v)
    {
        return Carbon::createFromFormat('d/m/Y H:i', $v);
    }

    public function dateOrNull($v)
    {
        try {
            return $this->date($v);
        } catch(\InvalidArgumentException $e) {
            return null;
        }
    }

    public function decimal($v)
    {
        $s = str_replace(',', '.', str_replace('.', '', $v));

        return floatval($s);
    }
}