<?php namespace App\Adon\Sanitizer;


use App\Adon\OCache\LightCache;
use Illuminate\Support\Str;

class Sanitizer
{

    /**
     * @var LightCache
     */
    private static $cacheInstance;

    /**
     * @return LightCache
     */
    private static function cache() {
        if (self::$cacheInstance) return self::$cacheInstance;
        return self::$cacheInstance = new LightCache();
    }

    // static

    /**
     * @param array $data
     * @param array $rules
     * @param bool $required
     * @return array
     * @throws SanitizerException
     */
    public static function run($data, $rules, $required=false)
    {
        $r = $data;

        foreach ($rules as $key=>$ruleList) {
            if ($required && !isset($data[$key])) {
                throw new SanitizerException("Parametro $key requerido.");
            } else {
                $safeValue = isset($data[$key]) ? $data[$key] : null;
                $r[$key] = self::process($safeValue, $ruleList);
            }
        }

        return $r;
    }

    /**
     * @param mixed $value
     * @param array $ruleList
     * @return mixed
     */
    private static function process($value, $ruleList)
    {
        if (!is_array($ruleList)) return self::process($value, [$ruleList]);

        $r = $value;
        foreach ($ruleList as $rule) {
            $r = self::apply($rule, $r);
        }

        return $r;
    }

    /**
     * @param string $caller
     * @param mixed $val
     * @return mixed
     */
    private static function apply($caller, $val)
    {
        // =string
        if (Str::startsWith($caller, "=")) return substr($caller, 1);

        list($class, $method) = mb_split('@', $caller);

        // let's cache the object, otherwise it would initialize all the damn time

        $instance = self::cache()
            ->remember($class, function() use ($class) { return new $class; });


        return $instance->{$method}($val);
    }

}