<?php namespace App\Adon\Sanitizer;


class VoidSanitizer
{
    public static $dash = 'App\Adon\Sanitizer@dash';
    public static $dot = 'App\Adon\Sanitizer@dot';

    public function dash($v)
    {
        return $v . '-';
    }

    public function dot($v)
    {
        return '.' . $v;
    }
}