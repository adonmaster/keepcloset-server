<?php namespace App\Adon\Repo;


use App\Blog;
use Carbon\Carbon;

class BlogRepo
{

    /**
     * @param $id
     * @param $title
     * @param $body
     * @param Carbon $published_at
     * @param $publisher_id
     * @return Blog|\Illuminate\Database\Eloquent\Model|static
     */
    public function save($id, $title, $body, Carbon $published_at, $publisher_id)
    {
        return Blog::updateOrCreate(
            compact('id'),
            compact('title', 'body', 'published_at', 'publisher_id')
        );
    }

    public function getWithRel()
    {
        return Blog::with('attachments', 'publisher')
                ->orderBy('published_at')
                ->get();
    }

    public function paginateWithRel()
    {
        return Blog::with('attachments', 'publisher')
            ->where('published_at', '<', Carbon::tomorrow())
            ->orderByDesc('published_at')
            ->paginate();
    }

    /**
     * @param int $id
     * @return Blog|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function findWithRel($id)
    {
        return Blog::with('attachments', 'publisher')->find($id);
    }

    public function destroy($blogId)
    {
        if ($model = Blog::find($blogId))
        {
            Repo::attachment()->removeFrom($model);

            $model->delete();
        }
    }
}