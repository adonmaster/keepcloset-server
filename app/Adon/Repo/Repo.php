<?php namespace App\Adon\Repo;


class Repo
{
    private static $cache = [];

    /**
     * @return RegistrationRepo
     */
    public static function registration() {
        return self::retrieve('registration', function() {
            return new RegistrationRepo();
        });
    }

    /**
     * @return GenericRepo
     */
    public static function generic()
    {
        return self::retrieve('generic', function() {
            return new GenericRepo();
        });
    }


    /**
     * @return UserRepo
     */
    public static function user()
    {
        return self::retrieve('user', function() {
            return new UserRepo();
        });
    }

    /**
     * @return AttachmentRepo
     */
    public static function attachment()
    {
        return self::retrieve('attachment', function() {
            return new AttachmentRepo();
        });
    }

    /**
     * @return BlogPublisherRepo
     */
    public static function blogPublisher()
    {
        return self::retrieve('blogPublisher', function() {
            return new BlogPublisherRepo();
        });
    }

    /**
     * @return AvatarRepo
     */
    public static function avatar()
    {
        return self::retrieve('avatar', function() {
            return new AvatarRepo();
        });
    }

    /**
     * @return BlogRepo
     */
    public static function blog()
    {
        return self::retrieve('blog', function() {
            return new BlogRepo();
        });
    }

    /**
     * @param $key
     * @param \Closure $cb
     * @return mixed
     */
    private static function retrieve($key, \Closure $cb)
    {
        if ( ! isset(self::$cache[$key])) self::$cache[$key] = $cb();

        return self::$cache[$key];
    }
}