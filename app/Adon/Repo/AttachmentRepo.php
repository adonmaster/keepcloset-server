<?php namespace App\Adon\Repo;


use App\AttachableModel;
use App\Attachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class AttachmentRepo
{
    /**
     * @param Model|AttachableModel $model
     * @param UploadedFile $file
     * @return Attachment
     */
    public function saveFor(Model $model, UploadedFile $file)
    {
        $server = 'local';
        $filename = $model->id . '_' . Str::random(24) . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('attachments',  $filename);
        $url = asset('/storage/attachments/' . $filename);

        $type = 'file';

        return $model->attachment()->updateOrCreate([], compact('server', 'type', 'path', 'url'));
    }

    /**
     * @param $attachable_type
     * @param $attachable_id
     * @param string $type
     * @param UploadedFile $file
     * @return Attachment|Model
     */
    public function create($attachable_type, $attachable_id, $type, UploadedFile $file)
    {
        $server = 'local';
        $filename = time() . '_' . Str::random(8) . '.' . $file->getClientOriginalExtension();
        $file->storeAs('attachments',  $filename);

        $path = storage_path('app/attachments/' . $filename);
        $url = asset('/storage/attachments/' . $filename);

        return Attachment::create(compact('attachable_type', 'attachable_id', 'server', 'type', 'path', 'url'));
    }

    /**
     * @param $attachable_type
     * @param $attachable_id
     * @param $type
     * @param $url
     * @return $this|Model
     */
    public function createWithUrl($attachable_type, $attachable_id, $type, $url)
    {
        $server = 'local';
        $path = '';
        return Attachment::create(compact('attachable_type', 'attachable_id', 'server', 'type', 'path', 'url'));
    }

    private function unlink($path) {
        if (file_exists($path)) {
            unlink($path);
        }
    }

    /**
     * @param Model|AttachableModel $model
     */
    public function removeFrom($model)
    {
        if (isset($model->attachment)) {
            if ($attachment = $model->attachment) {
                $this->unlink(storage_path('app/' . $attachment->path));
            }
            $model->attachment()->delete();
        }

        if (isset($model->attachments)) {
            $model->attachments()->each(function($attachment) {
                $this->unlink(storage_path('app/' . $attachment->path));
            });
            $model->attachments()->delete();
        }

    }

    public function remove($attachmentId)
    {
        if ($model = Attachment::find($attachmentId)) {
            $this->unlink($model->path);
            $model->delete();
        }
    }

    public function syncIds(array $attachmentIds, $attachable_type, $attachable_id)
    {
        $attrs = compact('attachable_type', 'attachable_id');

        // update to correct morph
        Attachment::whereIn('id', $attachmentIds)->update($attrs);

        // syncing
        $currentIds = Attachment::where($attrs)->get('id')->pluck('id');
        $deleteIds = array_diff($currentIds->toArray(), $attachmentIds);
        foreach ($deleteIds as $id) {
            $this->remove($id);
        }
    }
}