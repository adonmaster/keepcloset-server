<?php namespace App\Adon\Repo;


use App\Exceptions\RepoException;

class BaseRepo
{
    /**
     * @param string $message
     * @throws RepoException
     */
    public function fail($message)
    {
        throw new RepoException($message);
    }

}