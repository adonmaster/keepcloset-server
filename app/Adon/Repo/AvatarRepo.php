<?php namespace App\Adon\Repo;


use App\Avatar;
use App\AvatarableModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class AvatarRepo
{

    /**
     * @param AvatarableModel $model
     * @param $file
     * @return Avatar|\Illuminate\Database\Eloquent\Model
     */
    public function saveTo($model, UploadedFile $file)
    {
        // remove before save
        $this->removeFrom($model);

        // save original file
        $filename = $model->id . '_' . Str::random(8) . '.' . $file->getClientOriginalExtension();
        $path = storage_path('app/avatars/' . $filename);
        \Image::make($file)->widen(500, function($constraint) {
            $constraint->upsize();
        })->save($path);
        $url = asset('/storage/avatars/' . $filename);

        // save thumb
        $filenameThumb = $model->id . '_thumb_' . Str::random(8) . '.' . $file->getClientOriginalExtension();
        $path_thumb = storage_path('app/avatars/' . $filenameThumb);
        $url_thumb = asset('/storage/avatars/' . $filenameThumb); // symbolic link, dont need 'app'

        // generate thumbnail
        \Image::make($file)->fit(200)->save($path_thumb);

        return $model->avatar()->updateOrCreate([], compact('path', 'url', 'path_thumb', 'url_thumb'));
    }

    /**
     * @param AvatarableModel $model
     */
    public function removeFrom($model)
    {
        /**
         * @var Avatar $avatar
         */
        if ($avatar = $model->avatar) {
            unlink($avatar->path_thumb);
            unlink($avatar->path);

            $model->avatar()->delete();
        }
    }
}