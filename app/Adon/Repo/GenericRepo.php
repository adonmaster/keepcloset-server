<?php namespace App\Adon\Repo;


use App\Generic;
use App\GenericItem;

class GenericRepo
{
    /**
     * @param string $parentName
     * @param array $itemBody
     * @param null|string $parentDescription
     * @param int $position
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function add($parentName, array $itemBody, $parentDescription=null, $position=0)
    {
        $parent = $this->saveParent($parentName, $parentDescription);

        return $parent->items()->create([
            'body' => $itemBody,
            'position' => $position
        ]);
    }

    /**
     * @param string $name
     * @param string $desc
     * @return Generic|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    private function saveParent($name, $desc)
    {
        $model = Generic::where(compact('name'))->first();
        if ( ! $model) return Generic::create(compact('name', 'desc'));

        if ($model->desc != $desc) $model->update(compact('desc'));

        return $model;
    }

    public function getFrom(array $parents)
    {
        return Generic::with('items', 'items.attachment', 'items.attachments')
            ->where(function($q) use ($parents) {
                if (count($parents)) $q->whereIn('name', $parents);
            })
            ->get();
    }

    /**
     * @param $parent_name
     * @param $id
     * @param $body
     * @param $position
     * @return GenericItem|\Illuminate\Database\Eloquent\Model|static
     */
    public function saveItem($parent_name, $id, $body, $position)
    {
        $parent = $this->saveParent($parent_name, null);
        $generic_id = $parent->id;

        return GenericItem::updateOrCreate(
            compact('id'),
            compact('generic_id', 'body', 'position')
        );
    }

    /**
     * @param int $itemId
     * @return GenericItem|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function findItemWithRel($itemId)
    {
        return GenericItem::with('attachment', 'attachments')
            ->find($itemId);
    }

    /**
     * @param int $itemId
     */
    public function destroyItem($itemId)
    {
        GenericItem::destroy($itemId);
    }

    public function positionItems(array $positions)
    {
        foreach ($positions as $id => $position) {
            GenericItem::where(compact('id'))->update(compact('position'));
        }
    }

    public function getItemsFrom($parentName)
    {
        $parent = Generic::with('items', 'items.attachment', 'items.attachments')
            ->where('name', $parentName)
            ->first();

        return $parent
            ? $parent->items
            : collect([]);
    }
}