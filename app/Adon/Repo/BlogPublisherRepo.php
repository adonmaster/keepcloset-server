<?php namespace App\Adon\Repo;


use App\BlogPublisher;

class BlogPublisherRepo
{

    public function getAll()
    {
        return BlogPublisher
            ::orderByDesc('id')
            ->get();
    }

    /**
     * @param int $id
     * @param string $name
     * @param string $insta
     * @return BlogPublisher|\Illuminate\Database\Eloquent\Model|static
     */
    public function save($id, $name, $insta)
    {
        return BlogPublisher
            ::updateOrCreate(compact('id'), compact('name', 'insta'));
    }

    /**
     * @param int $id
     * @return BlogPublisher|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function findWithRel($id)
    {
        return BlogPublisher::find($id);
    }

    public function destroy($blogPublisherId)
    {
        BlogPublisher
            ::where('id', $blogPublisherId)
            ->delete();
    }

    public function countBlogFor($modelPublisherId)
    {
        $publisher = BlogPublisher::find($modelPublisherId);

        return $publisher ? $publisher->posts()->count() : 0;
    }
}