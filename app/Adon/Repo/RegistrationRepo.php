<?php namespace App\Adon\Repo;


use App\Registration;
use Carbon\Carbon;

class RegistrationRepo extends BaseRepo
{

    /**
     * @param $email
     * @return Registration
     */
    public function create($email)
    {
        /**
         * @var Registration $model
         */
        $model = Registration::where(compact('email'))->first();

        if ($model)
        {
            if ($model->expired_at->isPast())
            {
                $model->expired_at = $this->generateExpiration();
                $model->code = $this->generateCode();
                $model->save();
                return $model;
            }

            return $model;
        }

        else
        {
            $model = Registration::init($email, $this->generateCode(), $this->generateExpiration());
            $model->save();

            return $model;
        }
    }

    /**
     * @return string
     */
    private function generateCode()
    {
        return strval(random_int(100000, 999999));
    }

    /**
     * @return Carbon
     */
    private function generateExpiration()
    {
        return Carbon::now()->addHours(2);
    }

    public function validate($email, $code)
    {
        /**
         * @var Registration $model
         */
        $model = Registration::where(compact('email'))->first();

        if ($model)
        {
            if ($model->code != $code) $this->fail("Código incorreto");
            if ($model->expired_at->isPast()) $this->fail("Código expirado. Gere-o novamente.");
        }

        else {
            $this->fail("Email não encontrado.");
        }
    }
}