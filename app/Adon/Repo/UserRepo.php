<?php namespace App\Adon\Repo;


use App\User;

class UserRepo
{

    /**
     * @param string $email
     * @param string $name
     * @param string $password
     * @param boolean $is_admin
     * @return User|\Illuminate\Database\Eloquent\Model|static
     */
    public function save($email, $name, $password, $is_admin=null)
    {
        $data = compact('email','password', 'name', 'is_admin');
        $data = array_filter($data, function($item) {
            return ! is_null($item);
        });

        return User::updateOrCreate(['email' => $email], $data);
    }
}