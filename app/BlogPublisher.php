<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BlogPublisher
 *
 * @package App
 * @property $id
 * @property string $name
 * @property string $insta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \App\Avatar $avatar
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPublisher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPublisher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPublisher query()
 * @mixin \Eloquent
 */
class BlogPublisher extends Model implements AvatarableModel
{
    protected $table = 'blog_publisher';
    protected $fillable = ['name', 'insta'];
    protected $dates = [];
    public $timestamps = true;
    protected $casts = [];
    protected $with = ['avatar'];


    # Relationships

    public function avatar()
    {
        return $this->morphOne(Avatar::class, 'avatarable');
    }

    public function posts()
    {
        return $this->hasMany(Blog::class, 'publisher_id', 'id');
    }
}
