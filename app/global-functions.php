<?php

function asAsset($path) {
    return \App\Adon\Ver::assetSecure($path);
}

function asAssetVer($path) {
    return \App\Adon\Ver::asset($path);
}

function isSecure() {
    return getenv('APP_SSL')=='true';
}

function secureIt($url) {
    if (isSecure()) {
        if (\Illuminate\Support\Str::startsWith($url, 'https:')) return $url;
        if (\Illuminate\Support\Str::startsWith($url, 'http:')) {
            return 'https' . substr($url, 4);
        }
        return 'https://' . $url;
    }

    return $url;
}