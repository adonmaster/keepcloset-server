<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attachment
 *
 * @package App
 * @property $id
 * @property $attachable_type
 * @property $attachable_id
 * @property $server
 * @property $type
 * @property $path
 * @property $url
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $attachable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment query()
 * @mixin \Eloquent
 */
class Attachment extends Model
{
    protected $table = 'attachments';
    protected $fillable = [
        'attachable_type', 'attachable_id', 'server', 'type', 'path', 'url'
    ];
    protected $hidden = ['path'];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = [
    ];
    protected $with = [];


    # Relationships

    public function attachable()
    {
        return $this->morphTo();
    }
}
