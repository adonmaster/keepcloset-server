<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Clockwork\Support\Laravel\ClockworkServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laracasts\Generators\GeneratorsServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        // Carbon internationalization
        \Carbon\Carbon::setLocale('pt-BR');
        setlocale(LC_TIME, 'pt-BR');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // urls
        $this->app->bind('path.public_html', function() {
            return base_path().'/public_html';
        });

        // storage
        $this->app->bind('path.public', function() {
            return base_path().'/public_html';
        });

        if ($this->app->environment() !== 'production')
        {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(GeneratorsServiceProvider::class);
            $this->app->register(ClockworkServiceProvider::class);
        }
    }
}
