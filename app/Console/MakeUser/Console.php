<?php namespace App\Console\MakeUser;


use App\Adon\Repo\Repo;
use Illuminate\Console\Command;

class Console extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adon:make-user {email} {name} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Salva usuário';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $email = $this->argument('email');
        $name = $this->argument('name');
        $password = $this->argument('password');

        Repo::user()->save($email, $name, $password);

        $this->info("Protinho: $email");
    }

}