<?php namespace App\Console\MakeJobConsole;

use Illuminate\Console\Command;

class Console extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adon:make-job {className} {--fields=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Modo de usar: Name/Space/PorraJob -> Name/Space/PorraJob.php(abstract DTO) + Name/Space/PorraJobHandler.php(handler)';

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var Generator
     */
    private $generator;

    /**
     * Create a new command instance.
     *
     * @param Parser $parser
     * @param Generator $generator
     * @return \App\Console\MakeJobConsole\Console
     */
    public function __construct(Parser $parser, Generator $generator)
    {
        parent::__construct();

        $this->parser = $parser;
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $class = $this->argument('className');
        $fields = $this->option('fields');

        $input = $this->parser->parse($class, $fields);

        if (file_exists($input->file_path) || file_exists($input->handler_path))
        {
            if (!$this->confirm('Arquivo jah existe, bonitao! Quer sobrescrever?'))
            {
                $this->error('rs.. vc eh uma piada.');

                return;
            }
        }

        $jobTemplate = __DIR__ . '/templates/job.template.txt';
        $handlerTemplate = __DIR__ . '/templates/handler.template.txt';

        $this->generator->generate($input, $jobTemplate, $handlerTemplate);

        $this->info('Acho que foi blza... VAI TRAMPAR PORRA!!!');
    }
}
