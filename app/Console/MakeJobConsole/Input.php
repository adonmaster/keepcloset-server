<?php namespace App\Console\MakeJobConsole;


class Input {


    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $name_handler;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var \string[]
     */
    public $fields;

    /**
     * @var string
     */
    public $file_path;

    /**
     * @var string
     */
    public $handler_path;

    /**
     * @param string $name
     * @param string $namespace
     * @param string[] $fields
     * @param string $file_path
     * @param $name_handler
     * @param string $handler_path
     */
    function __construct($name, $namespace, $fields, $file_path, $name_handler, $handler_path)
    {
        $this->fields = $fields;
        $this->name = $name;
        $this->name_handler = $name_handler;
        $this->namespace = $namespace;
        $this->file_path = $file_path;
        $this->handler_path = $handler_path;
    }
}