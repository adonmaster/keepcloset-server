const webpack = require('webpack');
const path = require('path');

const public_path = './public_html';

// https://vue-loader.vuejs.org/migrating.html
const VueLoaderPlugin = require('vue-loader/lib/plugin');

let entries = ['vue-core', 'vue-admin'];

const globalEntry = {};
(function() {
    for (let cont=0; cont<entries.length; cont++) {
        const item = entries[cont];
        globalEntry[item] = './' + item + '.js';
    }
})();

module.exports = {

    context: path.resolve(__dirname, './resources/js/vue'),

    entry: globalEntry,

    output: {
        path: path.resolve(__dirname, public_path+'/js/vue'),
        filename: '[name].min.js',
        publicPath: public_path
    },

    plugins: [new VueLoaderPlugin()],

    resolve: {
        modules: ["node_modules", "bower_components"],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },

    module: {
        rules: [

            // babel
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ["@babel/preset-env"],
                            plugins: ["@babel/transform-runtime"]
                        }
                    }
                ],
                exclude: file => (
                    /node_modules/.test(file)
                    && !/\.vue\.js/.test(file)
                )
            },

            // vue
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                // exclude: /(node_modules|bower_components)/
            },

            // css
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            },

            // less
            {
                test: /\.less$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },

            // scss
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },

            // some images
            {
                test: /\.(png|jpg|gif|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192
                        }
                    }
                ]
            }
        ]
    }
};


// Environment

const debug = process.env.NODE_ENV !== 'production';
console.log('*************************************');
console.log('************* '+ (debug ? 'Debug' : 'Production') +' *************');
console.log('*************************************\n');

// webpack mode
module.exports.mode = debug ? 'development' : 'production';

// external module
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

if ( ! debug)
{
    module.exports.optimization = {
        minimizer: [
            // we specify a custom UglifyJsPlugin here to get source maps in production
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                /*uglifyOptions: {
                    compress: false,
                    ecma: 6,
                    mangle: true
                },*/
                sourceMap: false
            })
        ]
    };

    module.exports.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        })
    );

}