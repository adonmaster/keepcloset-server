@extends('master')

@section('content')

    <div class="container-fluid">
        <div class="row">

            <div class="col-12 mt-5 text-center mb-4">
                <h1>
                    Seja bem vindo!
                </h1>

                <p class="mdc-text-grey-500">
                    Conheça o mais completo e inovador organizador de closet!
                </p>

            </div>

            @include('mobile/__support-footer')

        </div>
    </div>

@endsection