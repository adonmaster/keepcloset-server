@extends('master')

@section('content')

    <div class="container-fluid">
        <div class="row">

            <div class="col-12 mt-5 text-center mb-4">
                <h1>
                    QUEM SOMOS NÓS
                </h1>

                @php
                    $qmSomosItems = \App\Adon\Repo\Repo::generic()->getItemsFrom('welcome.qm-somos');
                @endphp


                <div class="row">
                    @foreach($qmSomosItems as $index => $item)
                        <div class="col-12 text-left mt-3">
                            <div class="media align-items-center">

                                <img src="{{ secureIt($item->attachment->url) }}" width="80" height="80"
                                     class="rounded-circle mr-4"
                                >

                                <div class="media-body">
                                    <h4 class="m-0">
                                        {{ $item->body['name'] }}
                                    </h4>
                                    <p class="mdc-text-grey-500 mt-2 mb-2">
                                        {!! $item->body['desc'] !!}
                                    </p>
                                    <a href="https://www.instagram.com/{{ trim($item->body['insta']) }}/" target="_blank">
                                        <i class="fa fa-instagram"></i> Visite meu perfil!
                                    </a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    @endforeach
                </div>

            </div>

            @include('mobile/__support-footer')
        </div>
    </div>

@endsection