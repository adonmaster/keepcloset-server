@extends('master')

@section('content')

    <div class="container-fluid">
        <div class="row">

            <div class="col-12 mt-5 text-center mb-4">
                <h1>
                    LIFESTYLE
                </h1>

                @include('__blog-list')

            </div>

            @include('mobile/__support-footer')
        </div>
    </div>

@endsection