@extends('master')

@section('content')

    <div class="container-fluid">
        <div class="row">

            <div class="col-12 mt-5 text-center mb-4">
                <h1>
                    PERGUNTAS FREQUENTES
                </h1>

                @php
                    $faqItems = \App\Adon\Repo\Repo::generic()->getItemsFrom('welcome.faq');
                @endphp

                <div class="row">
                    @foreach($faqItems as $index => $item)
                        <div class="col-12 text-left">
                            <hr>
                            <div class="media align-items-top">
                                <span class="fw800 mr-4 fs18">#{{$index + 1}}</span>
                                <div class="media-body ">
                                    <div class=" ">
                                        {!! $item->body['question'] !!}
                                    </div>
                                    <div class=" mt-2 fw700 mdc-text-grey-500">
                                        {!! $item->body['answer'] !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

            @include('mobile/__support-footer')
        </div>
    </div>

@endsection