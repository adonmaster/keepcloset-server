@extends('master')

@section('content')

    @include('__menu')

    <div class="container-fluid mdc-bg-grey-50" style="padding: 100px 0 40px 0;">
        <div class="container">

            <h1>LIFESTYLE</h1>
            <hr>

            @include('__blog-list')

        </div>
    </div>

@endsection