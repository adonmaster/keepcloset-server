@extends('master')

@section('content')

    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url('/admin') }}">
            KeepCloset - Admin
        </a>
        <div>
            <a href="{{ route('home') }}" target="_blank" class="mdc-text-pink-500">
                <i class="fa fa-arrow-right fa-fw"></i>
                Visitar site
            </a>
        </div>
    </nav>

    <div id="vue-app">

        <admin-view></admin-view>

    </div>

@endsection

@section('scripts')
    <script src="{{ App\Adon\Ver::asset('js/vue/vue-core.min.js') }}"></script>
    <script src="{{ App\Adon\Ver::asset('js/vue/vue-admin.min.js') }}"></script>
@endsection