@extends('mail.template')

@section('content-title')
    Contato do Site keepCloset.com
@endsection


@section('content-subtitle')

@endsection


@section('content-body')

    <div style="font-size: 12pt">
        <div style="margin: 10px 0 10px 0">
            <strong>
                <small>email: </small>
            </strong>
            {{ $email }}
        </div>
        <div style="margin: 10px 0 10px 0">
            <strong>
                <small>assunto: </small>
            </strong>
            {{ $subject }}
        </div>
        <div style="margin: 10px 0 10px 0">
            <strong>
                <small>corpo da mensagem: </small>
            </strong>
            <p style="padding: 2px 0 2px 0; line-height: 1.2em">
                {!! nl2br($body) !!}
            </p>
        </div>
    </div>

@endsection