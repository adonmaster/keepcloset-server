@extends('master')

@section('content')

    @include('__menu')

    {{--init--}}
    <div id="container-init" class="container-fluid" data-parallax="scroll" data-image-src="{{ asAsset('imgs/bknd-closet-1.jpg') }}"
         style="padding: 150px 0 70px 0;">

        <div class="container pt-5 pb-5">
            <div class="row justify-content-md-center">
                <div class="col-md-6 text-center">

                    <img class="pt-2" src="{{ asAsset('imgs/logo.png') }}" alt="Logo" style="max-width: 100%">

                    <p class="mdc-text-pink-600 pb-3 fw700 mt-3">
                        O seu estilo em suas mãos
                    </p>

                    <p class="text-center">
                        <a href="https://itunes.apple.com/br/app/keepcloset/id1459910681" target="_blank">
                            <img src="{{ asAsset('imgs/appstore-art.svg') }}"
                                 width="150" alt="Download">
                        </a>
                        <a href="#" target="">
                            <img src="{{ asAsset('imgs/gplay-art.png') }}"
                                 width="190" alt="Download">
                        </a>
                    </p>

                </div>
            </div>
        </div>
    </div>

    {{--flash render--}}
    @if(\App\Adon\Flash\Flash::has())
    <div class="container-fluid mt-3">
        <div class="container">
            {!! \App\Adon\Flash\Flash::render() !!}
        </div>
    </div>
    @endif
    {{--flash render--}}

    {{--ebook--}}
    <div class="container-fluid" data-parallax="scroll" data-image-src="{{ asAsset('imgs/bknd-ebook.jpg') }}">
        <div class="container pt-5 pb-5 text-white">
            <h1 class="mb-5">EBOOK</h1>

            <p class="mdc-text-pink-500 fw700 text-center">
                Esse eBook vai te ajudar a montar looks e explorar melhor seu guarda-roupa. Escrito pela consultora de Imagem Tete Reinaldim.
            </p>

            {!! Form::open(['route' => 'ebook', 'class' => 'js-form-hide-submit']) !!}
                <div class="form-group">
                    <input class="form-control form-control-lg text-center"
                           name="email" type="email" required
                            placeholder="Insira seu email aqui...">
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-primary btn-lg">
                        Acessar eBook
                    </button>
                </div>
            {!! Form::close() !!}

        </div>
    </div>

    {{--who uses--}}
    <div id="container-qm-usa" class="container-fluid mdc-bg-grey-50">
        <div class="container pt-5 pb-3">
            <h1 class="mb-5">
                QUEM USA
            </h1>

            <div class="welcome-quem-usa-carousel slick-carousel">

                {{--item--}}
                @php
                    $quemUseItems = \App\Adon\Repo\Repo::generic()->getItemsFrom('welcome.qm-usa');
                @endphp

                @foreach($quemUseItems as $item)
                <div class="text-center item">
                    <div>
                        <img src="{{ secureIt($item->attachment->url) }}" width="120" height="120">
                    </div>
                    <h4 class="m-0">
                        {{ $item->body['name'] }}
                    </h4>
                    <p class="mdc-text-grey-500 m-0">
                        {{ $item->body['desc'] }}
                    </p>
                    <a href="https://www.instagram.com/{{ trim($item->body['insta']) }}/" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                </div>
                @endforeach

            </div>

        </div>
    </div>


    {{--faq--}}
    <div id="container-faq" class="container-fluid mdc-bg-blue-grey-50">
        <div class="container pt-5 pb-5">
            <h1 class="mb-5">
                FAQ
            </h1>

            @php
                $faqItems = \App\Adon\Repo\Repo::generic()->getItemsFrom('welcome.faq');
            @endphp

            <ul class="list-group list-group-flush" >
                @foreach($faqItems as $index => $item)
                <li class="list-group-item">
                    <div class="media align-items-center">
                        <i class="fa fa-angle-right fa- mr-3"></i>
                        <div class="media-body mt-3 mb-3">
                            <a href="#faqItem{{$index}}" class="link-black " data-toggle="collapse">
                                {{ $item->body['question'] }}
                            </a>
                            <div class="collapse pl-3 mdc-text-pink-700 fw700" id="faqItem{{$index}}">
                                <div class="pt-3">
                                    {!! nl2br($item->body['answer'])  !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>

        </div>
    </div>

    {{--quem somos--}}
    <div id="container-qm-somos" class="container-fluid mdc-bg-grey-50">
        <div class="container pt-5 pb-5">
            <h1 class="mb-5">
                QUEM SOMOS
            </h1>

            @php
                $qmSomosItems = \App\Adon\Repo\Repo::generic()->getItemsFrom('welcome.qm-somos');
            @endphp

            @foreach($qmSomosItems as $index => $item)
            <div class="media align-items-center pt-3 pb-3">

                @php
                    $isLeft = ($index % 2) == 0;
                    $isRight = !$isLeft;
                    $isLast = $index == count($qmSomosItems)-1;
                @endphp

                @if($isLeft)
                <img src="{{ secureIt($item->attachment->url) }}"
                     class="rounded-circle"
                     width="100" height="100"
                     style="object-fit: cover"
                    >
                @endif

                <div class="media-body pr-4 pl-4 {{ $isLeft ? 'text-left' : 'text-right' }}">
                    <h5 class="fw700 mdc-text-pink-700">{{ $item->body['name'] }}</h5>
                    {!! nl2br($item->body['desc']) !!}

                    <div class="mt-3">
                        <a class="btn btn-outline-primary btn-sm" href="https://www.instagram.com/{{ trim($item->body['insta']) }}/" target="_blank">
                            <i class="fa fa-instagram fa-fw"></i> Venha me seguir!
                        </a>
                    </div>
                </div>

                @if($isRight)
                    <img src="{{ secureIt($item->attachment->url) }}"
                         class="rounded-circle"
                         width="100" height="100"
                         style="object-fit: cover"
                    >
                @endif
            </div>

            @if(! $isLast)
                <hr>
            @endif

            @endforeach

        </div>
    </div>



    {{--contato--}}
    <div id="container-contact" class="container bg-white">
        <div class="container pt-5 pb-5">
            <h1 class="mb-5">
                CONTATO
            </h1>

            <p class="fs13">
                Em que podemos ajudar?
            </p>

            {{--flash render--}}
            @if(\App\Adon\Flash\Flash::has())
                <div class="container-fluid mt-3">
                    <div class="container">
                        {!! \App\Adon\Flash\Flash::render() !!}
                    </div>
                </div>
            @endif
            {{--flash render--}}

            {{--form--}}
            {!! Form::open(['route' => 'welcome.contact', 'class' => 'mt-5']) !!}
                <div class="row">
                    {{--email--}}
                    <div class="col-md-6">
                        @component('components.bs.input-text', [
                            'field' => 'email', 'label' => 'Email *', 'type' => 'email',
                        ]) @endcomponent
                    </div>

                    {{--subject--}}
                    <div class="col-md-6">
                        @component('components.bs.input-text', [
                            'field' => 'subject', 'label' => 'Assunto *',
                        ]) @endcomponent
                    </div>

                    {{--body--}}
                    <div class="col-12">
                        @component('components.bs.text-area', [
                            'field' => 'body', 'label' => 'Mensagem *'
                        ]) @endcomponent
                    </div>
                    <div class="col-12">
                        <div class="form-group text-right">
                            <button class="btn btn-primary">
                                <i class="fa fa-check"></i> Enviar
                            </button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>


    {{--footer--}}
    @include('__footer')

@endsection

@section('scripts')
    <script>
        jQuery(function() {

            // hide ebook form button after submit
            $('form.js-form-hide-submit').submit(function(e) {
                let button = $(e.target).find('button');
                $(button).hide('slow');
                return true;
            });

            // slick 'quem usa'
            $('.slick-carousel').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });


            $('a.smooth-scroll')
                .click(function (event) {
                    // On-page links
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000, function () {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                }
                                ;
                            });
                        }
                    }
                });

            // reveal
            let sr = ScrollReveal();

            [
                'h1',

                '#container-init img',

                '#container-faq li',

                '#container-qm-somos .media',

                '#container-contact p',
                '#container-contact .form-group'

            ].forEach(function(i) {
                if ($(i).length===0) console.log('missing: ' + i);
                sr.reveal(i, { delay: 0, scale: 0.3 });
            });
        })
    </script>
@endsection