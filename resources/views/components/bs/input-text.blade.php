{{--

Input-Text Component
fields:
    *field
    type='text'
    label=null
    ph=null
    ac='off'
    inputClass=''
--}}

@php
    $type = isset($type) ? $type : 'text';
    $label = isset($label) ? $label : null;
    $ph = isset($ph) ? $ph : null;
    $ac = isset($ac) ? $ac : null;
    $inputClass = $inputClass ?? '';

    $old = old($field);
    $pristine = !$old && !$errors->has($field);
    $validationClass = $pristine ? '' : ($errors->has($field) ? 'is-invalid' : 'is-valid')
@endphp

<div class="form-group">

    @if($label)
        <label for="{{ $field }}">{{ $label }}</label>
    @endif

    <input name="{{ $field }}" id="{{ $field }}" type="{{ $type }}"
           class="form-control {{ $validationClass }} {{$inputClass}}"
           value="{{ $old }}"
           placeholder="{{ $ph }}">

    <div class="invalid-feedback">
        {{ $errors->first($field) }}
    </div>

</div>
