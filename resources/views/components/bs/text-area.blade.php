{{--

Text-Area Component
fields:
    *field
    label=null
    ph=null
--}}

@php
    $label = isset($label) ? $label : null;
    $ph = isset($ph) ? $ph : null;
    $rows = isset($rows) ? $rows : 3;

    $old = old($field);
    $pristine = !$old && !$errors->has($field);
    $validationClass = $pristine ? '' : ($errors->has($field) ? 'is-invalid' : 'is-valid')
@endphp

<div class="form-group">

    @if($label)
        <label for="{{ $field }}">{{ $label }}</label>
    @endif

    <textarea class="form-control {{ $validationClass }}"
          name="{{$field}}"
          id="{{$field}}"
          rows="{{$rows}}"
          placeholder="{{ $ph }}"
        >{{ $old }}</textarea>

    <div class="invalid-feedback">
        {{ $errors->first($field) }}
    </div>

</div>
