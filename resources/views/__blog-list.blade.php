<div class="row">
    @foreach($blog as $post)
        <div class="col-md-6 mt-4">
            <div class="card border-secondary">
                <div class="card-body text-secondary">

                    <h2>{{$post->title}}</h2>
                    <div id="subtitle">
                                <span class="float-right mdc-text-grey-500">
                                    {{ $post->published_at->diffForHumans() }}
                                </span>
                        <small>
                            por:
                            <a class="fw700 link-black"
                               href="https://www.instagram.com/{{$post->publisher->insta}}/"
                               target="_blank"
                            >
                                <img
                                        src="{{$post->publisher->avatar->url_thumb}}"
                                        width="24px" height="24px"
                                        style="object-fit: cover; border: 1px solid dimgrey"
                                        class="rounded-circle">
                                {{$post->publisher->name}}
                            </a>
                        </small>
                    </div>
                    <hr>

                    <div id="post-body">
                        {!! $post->p()->body() !!}
                    </div>

                </div>

            </div>
        </div>
    @endforeach
</div>