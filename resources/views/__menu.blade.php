<div id="menu" class="bg-dark fixed-top">

    <nav class="navbar navbar-collapse navbar-expand-md navbar-light bg-white">

        <div class="container">
            <a class="navbar-brand" href="{{ URL::to('/') }}">
                <img src="{{ asset('android-icon-48x48.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="welcome-menu navbar-nav ff-primary fw700 nav-fill w-100">
                    <li class="nav-item {{ request()->is('/') ? 'active' : '' }}">
                        <a class="nav-link smooth-scroll"
                           href="{{ request()->is('/') ? '#container-init' : route('home') }}">
                            PRINCIPAL
                        </a>
                    </li>
                    <li class="nav-item pink {{ request()->is('blog') ? 'active' : '' }}">
                        <a class="nav-link smooth-scroll" href="{{ route('welcome.blog') }}">
                            LIFESTYLE <i class="fa fa-angle-double-right"></i>
                        </a>
                    </li>
                    <li class="nav-item pink {{ request()->is('tutorials') ? 'active' : '' }}">
                        <a class="nav-link smooth-scroll" href="{{ route('welcome.tutorials') }}">
                            TUTORIAIS <i class="fa fa-angle-double-right"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smooth-scroll"
                           href="{{ request()->is('/') ? '' : route('home') }}#container-qm-usa">
                            QUEM USA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smooth-scroll"
                           href="{{ request()->is('/') ? '' : route('home') }}#container-faq">
                            FAQ
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smooth-scroll"
                           href="{{ request()->is('/') ? '' : route('home') }}#container-qm-somos">
                            SOBRE NÓS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smooth-scroll"
                           href="{{ request()->is('/') ? '' : route('home') }}#container-contact">
                            CONTATO
                        </a>
                    </li>
                </ul>
            </div>
        </div> {{--container--}}

    </nav>

</div>
