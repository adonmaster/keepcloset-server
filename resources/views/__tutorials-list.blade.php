@php
    $tutorials = \App\Adon\Repo\Repo::generic()->getItemsFrom('tutorials');
@endphp

<div class="row">
    @foreach($tutorials as $index => $item)
        <div class="col-md-4">
            <div class="media align-items-center mt-5">
                            <span class="ff-secondary mdc-text-pink-700" style="font-size: 48px">
                                #{{  $index + 1 }}
                            </span>

                <div class="media-body ml-3">
                    {{--title--}}
                    <span class="fw700 fs13 ff-primary">{{$item->body['title']}}</span>

                    {{--embeded--}}
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{$item->body['youtube']}}" allowfullscreen></iframe>
                    </div>

                    {{--desc--}}
                    <small class="mdc-text-grey-500">
                        {!! $item->body['desc'] !!}
                    </small>
                </div>
            </div>
        </div>
    @endforeach
</div>