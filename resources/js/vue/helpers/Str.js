import Num from "./Num";

let Str = {

    /**
     *
     * @param {int} len
     * @param {int} bits
     * @returns {string}
     */
    random(len, bits=36)
    {
        bits = bits || 36;
        var outStr = "", newStr;
        while (outStr.length < len)
        {
            newStr = Math.random().toString(bits).slice(2);
            outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)));
        }
        return outStr.toUpperCase();
    },

    /**
     * @param {int} len
     * @param {string} charSet
     * @returns {string}
     */
    randomFrom(len=6, charSet=null)
    {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz,randomPoz+1);
        }
        return randomString;
    },

    /**
     *
     * @param {string} s
     * @param {bool} onlyFirstLetter
     * @returns {string}
     */
    ucwords(s, onlyFirstLetter=false) {
        var str = s.toLowerCase();
        let pattern = onlyFirstLetter ? /(^[\w\p{M}])/ : /(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g;
        return str.replace(pattern, s => s.toUpperCase());
    },

    /**
     * @param {number} qty
     * @param {string} plural
     * @param {string} single
     * @param {string} zero
     * @returns {string}
     */
    plural(qty, plural, single, zero='') {
        if (qty <= 0) return zero;
        if (qty == 1) return single;
        return plural;
    },
    
    coalesce(...strs) {
        for (let s in strs) {
            if (s && s.length) return s;
        }
    },

    filesize(n)
    {
        const kb = 1024;
        const mb = kb * 1024;
        const gb = mb * 1024;

        if (n < kb) return '< 1kb';
        if (n < mb) return Num.decimal(n/kb, 0, 1) + 'kb';
        if (n < gb) return Num.decimal(n/mb, 0, 1) + 'mb';

        return Num.decimal(n/gb, 0, 1) + 'gb';
    },

    /**
     * @param str
     * @param rule
     * @returns {boolean}
     */
    wildcard(str, rule) {
        if (~rule.indexOf('*')) {
            return new RegExp("^" + rule.split("*").join(".*") + "$").test(str);
        }
        return str==rule;
    }

};

export default Str;