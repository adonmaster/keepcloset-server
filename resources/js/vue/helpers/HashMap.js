export default class HashMap {

    /**
     * @returns {HashMap}
     */
    static f() {
        return new HashMap();
    }

    constructor() {
        this.__cache = [];
    }

    set(key, value) {
        let obj = {key, value};
        const index = this.__cache.findIndex(({key:k}) => k===key);
        if (~index) {
            this.__cache[index] = obj;
        } else {
            this.__cache.push(obj);
        }
    }

    get(key, def=null) {
        const index = this.__cache.findIndex(({key:k}) => k===key);
        if (~index) {
            return this.__cache[index].value;
        }
        return def;
    }

    clear() {
        this.__cache = [];
    }

    toObject() {
        let r = {};
        this.__cache.forEach(({key, value}) => r[key] = value);
        return r;
    }

}