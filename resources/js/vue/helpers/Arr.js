const Arr = {

    /**
     *
     * @param array
     * @param indexA
     * @param indexB
     * @returns {Array.<*>|boolean}: Ou retorna o array modificada, ou ===false
     */
    aSwitch(array, indexA, indexB) {
        if (array.length > 1) {
            let check = ii => { return ii>=0 && ii<=array.length-1 };
            if (check(indexA) && check(indexB)) {
                let tmp = array[indexA];
                array[indexA] = array[indexB];
                array[indexB] = tmp;
                return array;
            }
        }
        return false;
    }

};

export default Arr;