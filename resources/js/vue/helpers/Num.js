const Num = {

    /**
     * @param {number} v
     * @param {number} minDigits
     * @param {number} maxDigits
     * @returns {string}
     */
    decimal(v, minDigits=2, maxDigits=2) {
        return v.toLocaleString('pt-BR', {minimumFractionDigits: minDigits, maximumFractionDigits: maxDigits});
    },

    /**
     *
     * @param {number} min
     * @param {number} max
     * @returns {number}
     */
    rand(min=1, max=10) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

};

export default Num;