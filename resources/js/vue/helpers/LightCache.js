export default class LightCache {

    /**
     *
     * @returns {LightCache}
     */
    static get i() {
        if (this.__instance) return this.__instance;
        return this.__instance = new this();
    }

    /**
     *
     * @returns {LightCache}
     */
    static f() {
        return new LightCache();
    }


    constructor() {
        this.__cache = [];
    }

    /**
     *
     * @param {string} name
     * @param {function} cb
     * @returns {*}
     */
    retrieve(name, cb) {
        let o = this.__cache.find(({name:n}) => n==name);
        if (!o) this.__cache.push(o = {name, result: cb()});
        return o.result;
    }

    /**
     *
     * @param {string|null} name
     */
    clear(name=null) {
        if (name) {
            this.__cache = this.__cache.sort(({name:n}) => n!=name);
        } else {
            this.__cache = [];
        }
    }
}