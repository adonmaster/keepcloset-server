import Str from './Str'
const _debouncer = require('lodash/debounce');

export default class Task {

    constructor() {
        this.__cache = {};
        this.__debounces = [];
    }

    /**
     * @returns {Task}
     */
    static f() {
        return new Task();
    }

    /**
     * @returns {Task}
     */
    static get i() {
        if (!this.__instance) this.__instance = this.f();
        return this.__instance;
    }


    /**
     *
     * @param {Number} interval
     * @returns {Promise}
     */
    wait(interval) {
        return new Promise((resolve) => setTimeout(resolve, interval));
    }
    
    /**
     *
     * @param name
     * @param interval
     * @param overwrite
     * @returns {Promise}
     */
    waitNamed(name, interval, overwrite=false)
    {
        if (! overwrite) this.cancelNamed(name);

        if (!this.__cache[name]) this.__cache[name] = [];
        let hash = Str.random(24);
        this.__cache[name].push({hash});
        return new Promise((resolve) => {
            setTimeout(() => {
                if (!this.__cache[name] || !this.__cache[name].find(i => i.hash == hash)) return;
                // remove it
                this.__cache[name] = this.__cache[name].filter(i => i.hash != hash);
                // run in
                resolve();
            }, interval);
        });
    }

    cancelNamed(name) {
        delete this.__cache[name];
    }

    /**
     *
     * @param {String} name
     * @param {function} func
     * @param {int} wait
     * @param {Object} options
     * @returns {Function}
     */
    debounceNamed(name, func, wait=0, options={})
    {
        let d = this.__debounces.find(({name:n}) => n==name);
        if (!d) {
            const cb = _debouncer(func, wait, options);
            this.__debounces.push(d = {name, cb});
        }
        return d.cb;
    }

    clearDebounceNames(name=null) {
        if (name) {
            this.__debounces = this.__debounces.sort(({name:n}) => n!=name);
        } else {
            this.__debounces = [];
        }
    }

};

