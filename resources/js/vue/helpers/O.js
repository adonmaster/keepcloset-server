let O = {

    /**
     * @param {string} path
     * @param {Object} obj
     * @param {*} def
     *
     * @returns {*}
     */
    get(obj, path, def = null) {
        //console.log('o call: ', obj, path, def);
        var keys = isNaN(path) ? path.split('.') : [path];
        if (!keys.length) return def;
        if (!obj || !((obj instanceof Object) || (obj instanceof Array))) return def;
        let key = keys.shift();

        // obj?
        if (obj.hasOwnProperty(key)) {
            let newObj = obj[key];
            if (keys.length) return O.get(newObj, keys.join('.'), def);
            return newObj;
        }
        return def;
    },

    /**
     *
     * @param {string} path
     * @param {Object} obj
     * @param {Function} accept
     * @param {*} def
     * @returns {*}
     */
    promise(obj, path, accept, def = null) {
        var r = this.get(obj, path, null);
        if (r === null) return def;
        return accept(r);
    },

    /**
     *
     * @param {Object} obj
     * @param {string} path
     * @param {*} value
     *
     * @returns {*}
     */
    set(obj, path, value) {
        let arr = isNaN(path) ? path.split('.') : [path];
        if (arr.length > 0 && obj) {
            var key = arr.shift();
            var last = obj;
            while (key && arr.length > 0) {
                if (!last.hasOwnProperty(key) || (typeof last[key] != "object")) last[key] = {};
                last = last[key];
                key = arr.shift();
            }
            last[key] = value;
            return value;
        }
        return null;
    },

    /**
     * @param {*} obj
     */
    is(obj) {
        return obj !== null && typeof obj === 'object'
    },

    /**
     * @param {Object|Array} o
     * @param {Array} fields
     * @return {Array|Object}
     */
    filter(o, fields)
    {
        if (!Arr.is(o)) return this.filter([o], fields)[0];

        return o.map(v => {
            var n = {};
            for (var a of args) n[a] = v[a];
            return n;
        });
    },

    /**
     *
     * @param {Object} o
     * @param {String[]} paths
     * @param {*|null} def
     */
    findValidString(o, paths, def=null)
    {
        for (let cont=0; cont<paths.length; cont++) {
            let path = paths[cont];
            let r = this.get(o, path, '');
            if (r.length) return r;
        }
        return def;
    },

    /**
     * @param {Object} o
     * @param {string} field
     * @param {Function|*} cbValue
     * @returns {Object}
     */
    setIfFalse(o, field, cbValue) {
        if (!o || typeof o != 'object') throw 'Objeto inválido!';
        if (o.hasOwnProperty(field) && o[field]) return o;

        o[field] = typeof cbValue == 'function' ? cbValue() : cbValue;
        return o;
    }

};

export default O;