import Cookies from 'js-cookie'

export default class Cookie {

    /**
     * @returns {Cookie}
     */
    static get i() {
        if (!this._instance) this._instance = new Cookie();
        return this._instance;
    }

    /**
     *
     * @param {String} key
     * @param {String} val
     * @returns {String}
     */
    set(key, val) {
        let r = Cookies.set(key, val, this._options);
        this._options = null;
        return r;
    }

    /**
     * @param {String} key
     * @param {boolean} v
     */
    setBoolean(key, v) {
        return this.set(key, v ? 'true' : 'false');
    }

    /**
     *
     * @param {String} key
     * @returns {boolean}
     */
    getBoolean(key) {
        return this.get(key, 'false')=='true';
    }

    /**
     * @param {String} key
     * @param {String} def
     */
    get(key, def=null) {
        let r = Cookies.get(key, this._options);
        this._options = null;
        if (r === undefined) return def;
        return r;
    }

    getObject(key, def=null) {
        let r = this.get(key, def);
        if (r) return JSON.parse(r);
        return def;
    }

    /**
     *
     * @param {String} key
     * @returns {*}
     */
    remove(key) {
        let r = Cookies.remove(key, this._options);
        this._options = null;
        return r;
    }

    // # Options

    /**
     *
     * @param {Object} options
     * @returns {Cookie}
     */
    o(options) {
        this._options = options;
        return this;
    }

    /**
     *
     * @param {Number} days
     * @returns {Cookie}
     */
    expires(days) {
        if (!this._options) this._options = {};
        this._options['expires'] = days;
        return this;
    }

}