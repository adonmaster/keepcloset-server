import O from "./O";
import Str from "./Str";
import Url from "./Url";


/**
 * Taking advantage of window.Url
 * @external axios
 *
 * Don't fucking use
 * `const axios = window.axios`
 * if the file responsible for loading these classes is not fully loaded, the constant will be nullified
 *
 */

let Request = {

    __validatorListeners: [],

    /**
     * @param {Function} action
     * @returns {string}
     */
    registerValidatorListener(action) {
        let key = Str.random(24) + '_' + (this.__validatorListeners.length + '');
        this.__validatorListeners.push({key, action});
        return key;
    },

    unregisterValidatorListener(key) {
        if (key===null) {
            this.__validatorListeners = [];
        } else {
            this.__validatorListeners = this.__validatorListeners.filter(({key:k}) => k!==key)
        }
    },

    __fireValidatorListeners(data) {
        this.__validatorListeners.forEach(v => v.action(data));
    },

    /**
     * @param e axios error
     * @returns {null|string}
     */
    translateError(e)
    {
        if (e && e.response) {
            if (e.response.status == 422) {
                this.__fireValidatorListeners(e.response.data);
                return null;
            }
            else if (e.response.status == 413) {
                return 'Arquivo grande demais para o servidor.';
            }
            else if (e.response.data) {
                return O.findValidString(e.response.data, ['message', 'exception'], e+'');
            }
        }
        return e+'';
    },

    /**
     * @param {string} uri
     * @param {Object} vue
     * @param {Object} params
     */
    get(uri, vue, params={})
    {
        let that = this;
        let v = vue || {};

        return new Promise(function(resolve, reject) {
            v.error = null;
            v.loading = true;

            axios.get(Url.i.to(uri), {params})
                .then(({data}) => {
                    v.loading = false;
                    that.__fireValidatorListeners({});
                    resolve(data);
                })
                .catch(e => {
                    v.loading = false;
                    v.error = that.translateError(e);
                    reject(e);
                })
        });
    },

    /**
     * @param {string} uri
     * @param {Object} paramData
     * @param {Object} vue
     */
    post(uri, paramData, vue)
    {
        const that = this;
        let v = vue || {};

        return new Promise(function(resolve, reject)
        {
            v.loading = true;
            v.error = null;

            let body = that.__preparePostBody(paramData);
            let headers = that.__prepareHeader(paramData);

            axios.post(Url.i.to(uri), body, {headers})
                .then(({data}) => {
                    v.loading = false;
                    that.__fireValidatorListeners({});
                    resolve(data);
                })
                .catch(e => {
                    v.loading = false;
                    v.error = that.translateError(e);
                    // and the reject from Form may reassign the error variable.
                    reject(e);
                });
        });
    },

    __paramDataHasFile(paramData) {
        let keys = Object.keys(paramData);
        if (keys.length <= 0) return false;

        return keys.reduce((a, key) => {
            let v = paramData[key];
            let isFile = (v || 1).constructor.name == 'File';
            let isFileList = (v || 1).constructor.name == 'FileList';
            return a || isFile || isFileList;
        }, false);
    },

    __preparePostBody: function(paramData, forceFile=false) {
        let hasFile = forceFile || this.__paramDataHasFile(paramData);
        if (hasFile) {
            // transform to FormData
            return tap(new FormData(), (fd) => {
                let keys = Object.keys(paramData);
                keys.forEach(key => {
                    let v = paramData[key];
                    if ((v || 1).constructor.name == 'FileList') {
                        Array.from(v).forEach((file, i) => fd.append(`${key}[${i}]`, file));
                    } else {
                        fd.append(key, v)
                    }
                });
            });
        }

        return paramData;
    },

    __prepareHeader: function(paramData, forceFile=false) {
        let hasFile = forceFile || this.__paramDataHasFile(paramData);
        if (hasFile) {
            return {'Content-Type': 'multipart/form-data'}
        }
        return {};
    },

    /**
     * @param {string} uri
     * @param {Object} paramData
     * @param {Object} vue
     */
    delete(uri, paramData, vue)
    {
        let that = this;
        let v = vue || {};

        return new Promise(function(resolve, reject)
        {
            v.loading = true;
            v.error = null;

            let headers = {};
            axios.delete(Url.i.to(uri), paramData, {headers})
                .then(({data}) => {
                    v.loading = false;
                    that.__fireValidatorListeners({});
                    resolve(data);
                })
                .catch(e => {
                    v.loading = false;
                    v.error = that.translateError(e);
                    // and the reject from Form may reassign the error variable.
                    reject(e);
                });
        });
    },

};


export default Request;