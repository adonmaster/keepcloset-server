let Dom = {

    findIndex(e) {
        let r = 0, node = e;
        while (node = node.previousElementSibling) r++;
        return r;
    }

};

export default Dom;