export default class Url {

    /**
     *
     * @returns Url
     */
    static get i() {
        if (this.instance) return this.instance;
        return this.instance = new Url();
    }

    constructor() {
        let query = document.head.querySelector('meta[name="base-url"]');
        if (!query) console.error('Q tal colocar a bosta do $BASE_URL? <meta name="base-url" content="{{ \\URL::to(\'/\') }}">');
        this.baseUrl = query.content;
    }

    startSlash(uri, slash='/') {
        if (uri.startsWith(slash)) return uri;
        return slash + uri;
    }

    removeStartSlash(uri) {
        return uri.replace(/^(\/)+/, '');
    }

    endSlash(uri, slash='/') {
        if (uri.endsWith(slash)) return uri;
        return uri + slash;
    }

    merge(base, uri) {
        return this.endSlash(base) + this.removeStartSlash(uri);
    }

    to(uri) {
        if (uri.startsWith('http')) return uri;
        return this.merge(this.baseUrl, uri);
    }
}