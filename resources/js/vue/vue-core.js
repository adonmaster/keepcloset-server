// vue
import Vue from 'vue';
window.Vue = Vue;

// axios
import axios from 'axios';
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('Carai! Use no Blade: <meta name="csrf-token" content="{{ csrf_token() }}">');
}

// moment
import moment from 'moment';
window.moment = moment;
moment.updateLocale('pt-br', {
    calendar: {
        lastDay : '[Ontem]',
        sameDay : '[Hoje]',
        nextDay : '[Amanhã]',
        lastWeek : '[Semana passada]',
        nextWeek : '[Próxima semana]',
        sameElse : 'L'
    }
});

// plugins

import TransitionFade from './components/transitions/fade.js'
Vue.component('transition-fade', TransitionFade);

import TransitionListFade from './components/transitions/list-fade.js'
Vue.component('transition-list-fade', TransitionListFade);

import LoadError from './components/bs/load-error'
Vue.component('load-error', LoadError);

import HttpMixin from './components/mixins/http-mixin';
Vue.mixin(HttpMixin);

import UtilMixin from './components/mixins/util-mixin';
Vue.mixin(UtilMixin);

import Flash from './components/directives/flash.js'
Vue.directive('flash', Flash);

// form fields & alerts

import InputGroup from './components/form/input-group'
Vue.component('input-group', InputGroup);

import InputText from './components/form/input-text'
Vue.component('input-text', InputText);

import InputTextArea from './components/form/input-text-area'
Vue.component('input-text-area', InputTextArea);

import InputDatetime from './components/form/input-datetime'
Vue.component('input-datetime', InputDatetime);

import InputSelect from './components/form/input-select'
Vue.component('input-select', InputSelect);

import InputRadio from './components/form/input-radio'
Vue.component('input-radio', InputRadio);

import InputFile from './components/form/input-file'
Vue.component('input-file', InputFile);

import AlertSuccess from './components/bs/alert-success'
Vue.component('alert-success', AlertSuccess);

import Pagination from './components/bs/pagination'
Vue.component('pagination', Pagination);

// global functions

require('./global-functions');

// Vue filters (after global-functions)

Vue.filter('nl2br', nl2br);