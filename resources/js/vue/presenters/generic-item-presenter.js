import BasePresenter from "./base-presenter";
import Url from "../helpers/Url";

const _get = require('lodash/get');

export default class GenericItemPresenter extends BasePresenter {

    get attachmentUrl() {
        return this.cache.retrieve('attachmentUrl', ()=>{
            return _get(this.model, 'attachment.url', Url.i.to('imgs/error.jpg'))
        })
    }

}