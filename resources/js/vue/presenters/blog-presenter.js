import BasePresenter from "./base-presenter";
import LightCache from "../helpers/LightCache";

export default class BlogPresenter extends BasePresenter {

    get body() {
        const s = this.model.body;
        if (s.length > 35) return s.substring(0, 35).trim() + '...';
        return this.model.body;
    }

    get publishedAtObject() {
        return this.cache.retrieve('publishedAtObject', ()=>{
            return moment(this.model.published_at);
        })
    }

    get publishedAt() {
        return this.publishedAtObject.format('DD/MM/YYYY');
    }

    get isPublished() {
        const tomorrow = moment([]).add(1, 'day').startOf('day');
        return this.publishedAtObject.isBefore(tomorrow);
    }

    get badgeAttachment() {
        const n = this.model.attachments.length;
        return {
            'badge-light': n==0,
            'badge-primary': n
        }
    }

}