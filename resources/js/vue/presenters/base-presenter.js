import LightCache from "../helpers/LightCache";

export default class BasePresenter {

    constructor(model) {
        this.model = model;
        this.cache = LightCache.f();
    }

    get updatedAt() {
        return this.cache.retrieve('updatedAt', ()=>moment(this.model.updated_at));
    }

    get createdAt() {
        return this.cache.retrieve('createdAt', ()=>moment(this.model.created_at))
    }

}