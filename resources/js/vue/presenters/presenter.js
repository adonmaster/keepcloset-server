import GenericPresenter from "./generic-presenter";
import GenericItemPresenter from "./generic-item-presenter";
import BlogPresenter from "./blog-presenter";

const Presenter = {

    _a(data, presenterClass, cbItemMapper=null)
    {
        // force array
        if (!data) return null;
        if (!Array.isArray(data)) return this._a([data], presenterClass)[0];

        return data.map(o => {

            if ( ! o.hasOwnProperty('p')) {
                Object.defineProperty(o, 'p', {value: new presenterClass(o)});
            }

            if (cbItemMapper) return cbItemMapper(o);

            return o;
        });
    },

    // constants

    /**
     *
     * @param data
     * @returns {*}
     */
    generic(data) {
        return this._a(data, GenericPresenter, i=>{
            if (i.items) {
                i.items = this.genericItem(i.items)
            } else {
                i.items = []
            }
            return i;
        })
    },

    /**
     *
     * @param data
     * @returns {*}
     */
    genericItem(data) {
        return this._a(data, GenericItemPresenter)
    },

    /**
     *
     * @param data
     * @returns {BlogPresenter}
     */
    blog(data) {
        return this._a(data, BlogPresenter)
    }

};

export default Presenter;