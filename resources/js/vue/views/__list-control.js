import SuSwapModel from "../models/SuSwapModel";

const _inRange = require('lodash/inRange');

const UndListControl = {

    template: `
    <div>
        <button class="btn btn-secondary btn-sm"
            :disabled="index<=0" 
            @click="swap(index, index-1)">
            <i class="fa fa-angle-up"></i>
        </button>
                                    
        <button class="btn btn-secondary btn-sm"
            :disabled="index>=data.length-1" 
            @click="swap(index, index+1)">
            <i class="fa fa-angle-down"></i>
        </button>
        
        <button class="btn btn-danger btn-sm" @click="destroy(index)">
            <i class="fa fa-close"></i>
        </button>
    </div>
    `,

    props: {
        index: {required: true, type: Number},
        data: {required: true, type: Array}
    },

    methods: {

        swap(ia, ib) {
            let n = this.data.length;
            if (_inRange(ia, n) && _inRange(ib, n)) {
                let tmp = this.data[ia];
                this.$set(this.data, ia, this.data[ib]);
                this.$set(this.data, ib, tmp);

                let positionModel = new SuSwapModel(ia, this.data[ia], ib, this.data[ib]);
                this.$emit('on-swapped', positionModel);
            }
        },

        destroy(id) {
            this.$emit('on-destroy', this.data[id])
        }
    }

};

export default UndListControl;