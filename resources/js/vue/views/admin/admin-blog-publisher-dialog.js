import rmodal from '../../components/modal/rmodal';

const _deepClone = require('lodash/cloneDeep');


const AdminBlogPublisherDialog = {

    components: {rmodal},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text 
                ref="input-name" 
                v-model="model.name" field="name" ph="" label="Nome"
                ></input-text>
    
        <input-group 
                ref="input-insta" field="insta" label="Instagram"
                v-model="model.insta" ph="ex.: adonio.design">
            <div class="input-group-prepend" slot="prepend">
                <span class="input-group-text">
                    <i class="fa fa-instagram"></i>
                </span>
            </div>
        </input-group>
        
        <!--attachment img-->
        <input-file 
            ref="input-img"
            accept="image/*"
            field="avatar_img" v-model="newImg" label="Arquivo de imagem"
            ></input-file>
        
        <!--attachment saved    -->
        <div v-show="model.avatar">
            <div class="badge badge-primary">
                <i class="fa fa-file-o"></i>
                Arquivo já salvo no servidor, faça upload novamente para substituí-lo
            </div>
        </div>   

        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            newImg: null
        }
    },

    methods: {

        open(model)
        {
            this.newImg = null;
            this.model = _deepClone(model) || {id: 0};

            this.$pristineIt('input-name', 'input-insta', 'input-img');

            this.$m.open().catch(()=>{});
        },

        submit() {
            this.model['avatar_img'] = this.newImg;
            this.$post('admin/json/blog/publisher', this.model)
                .then(d => {
                    this.$emit('on-saved', d);
                    this.$m.close();
                })
        },

        cancel() {
            this.$m.close();
        },
    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        }
    }
};

export default AdminBlogPublisherDialog;