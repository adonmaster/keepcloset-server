import rmodal from '../../components/modal/rmodal';
import Task from "../../helpers/Task";
import AttachmentsView from "../attachment/attachments-view";

const _deepClone = require('lodash/cloneDeep');

const AdminAdDialog = {

    components: {rmodal, AttachmentsView},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text 
                ref="input-title" 
                v-model="body.title" field="title" ph="" label="Título"
                ></input-text>
    
        <input-text 
                ref="input-link" 
                v-model="body.link" field="link" ph="" label="Link"
                ></input-text>
                
        <input-text-area
                ref="input-desc"
                v-model="body.desc" field="desc" label="Descrição"
            ></input-text-area>

        <hr>
        
        <attachments-view 
            attachable-type="App\\Generic" 
            :attachable-id="model.id"
            :data="model.attachments"
            @on-added="onSavedAttachment"
            @on-destroyed="onDestroyedAttachment"
            ></attachments-view>


        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    props: {
        parentName: {required: true}
    },

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            body: {}
        }
    },

    methods: {

        open(model, position)
        {
            this.model = _deepClone(model) || {id: 0, position, attachments: []};
            this.body = this.model.body || {};

            this.$pristineIt('input-title', 'input-desc', 'input-link');

            this.$m.open().catch(()=>{});
        },

        _validate() {
            return new Promise((resolve, reject)=>{
                Task.i.wait(850)
                    .then(()=>{
                        if (! this.body.title) { reject('Título não preenchido.') }
                        else if (! this.body.desc) { reject('Descrição não preenchida.') }
                        else if (! this.body.link) { reject('Link não preenchido.') }
                        else {
                            resolve();
                        }
                    });
            });
        },

        submit() {
            this.loading = true;
            this.error = null;
            this._validate()
                .then(()=>{
                    this.loading = false;
                    this._postData();
                })
                .catch(err => {
                    this.error = err;
                    this.loading = false;
                });

        },

        _postData()
        {
            const params = {
                'parent_name': this.parentName,
                'id': this.model.id,
                'position': this.model.position,
                'body': this.body,
                'attachments': this.model.attachments.map(({id}) => id)
            };

            this.$post('admin/json/generics', params)
                .then(d => {
                    this.$emit('on-saved', d);
                    this.$m.close();
                });
        },

        cancel() {
            this.$m.close();
        },

        onSavedAttachment(model) {
            this.model.attachments.push(model);
        },

        onDestroyedAttachment(modelId) {
            this.model.attachments = this.model.attachments.filter(({id}) => modelId!=id)
        },

    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        }
    }

};

export default AdminAdDialog;