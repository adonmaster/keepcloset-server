import rmodal from '../../components/modal/rmodal';
import Task from "../../helpers/Task";


const _deepClone = require('lodash/cloneDeep');

const AdminTutorialsDialog = {

    components: {rmodal},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text 
                ref="input-title" 
                v-model="body.title" field="title" ph="" label="Título"
                ></input-text>
    
        <input-text-area
                ref="input-desc"
                v-model="body.desc" field="desc" label="Descrição"
            ></input-text-area>
        
        <input-group 
                ref="input-youtube" field="youtube" label="Link Youtube Embed"
                v-model="body.youtube" ph="https://www.youtube.com/embed/V57P9Bw2Fe0">
            <div class="input-group-prepend" slot="prepend">
                <span class="input-group-text">
                    <i class="fa fa-youtube-play"></i>
                </span>
            </div>
        </input-group>

        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    props: {
        parentName: {required: true}
    },

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            body: {}
        }
    },

    methods: {

        open(model, position)
        {
            this.newImg = null;
            this.model = _deepClone(model) || {id: 0, position};
            this.body = this.model.body || {};

            this.$pristineIt('input-title', 'input-desc', 'input-youtube');

            this.$m.open().catch(()=>{});
        },

        _extractYoutubeEmbedId(str) {
            if (str) {
                let pattern = /^http(?:s)?:\/\/www.youtube.com\/embed\/(.+)?.+$/;
                let results = pattern.exec(str);
                if (results && results.length) return results[0];
            }
            return false;
        },

        _validate() {
            return new Promise((resolve, reject)=>{
                Task.i.wait(850)
                    .then(()=>{
                        if (! this.body.title) { reject('Título não preenchido.') }
                        else if (! this.body.desc) { reject('Descrição não preenchida.') }
                        else if (! this._extractYoutubeEmbedId(this.body.youtube)) { reject('Link do youtube inválido.') }
                        else {
                            resolve();
                        }
                    });
            });
        },

        submit() {
            this.loading = true;
            this.error = null;
            this._validate()
                .then(()=>{
                    this.loading = false;
                    this._postData();
                })
                .catch(err => {
                    this.error = err;
                    this.loading = false;
                });

        },

        _postData()
        {
            const params = {
                'parent_name': this.parentName,
                'id': this.model.id,
                'position': this.model.position,
                'body': this.body
            };

            this.$post('admin/json/generics', params)
                .then(d => {
                    this.$emit('on-saved', d);
                    this.$m.close();
                });
        },

        cancel() {
            this.$m.close();
        },
    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        }
    }

};

export default AdminTutorialsDialog;