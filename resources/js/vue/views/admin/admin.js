import Cookie from "../../helpers/Cookie";
import AdminFrontPageView from "./admin-front-page";
import AdminTutorialsView from "./admin-tutorials";
import AdminBlogView from "./admin-blog";
import AdminAdView from "./admin-ad";


const AdminView = {

    components: {},

    //language=HTML
    template: `
        <div class="row no-gutters">
            
            <!--menu-->
            <div class="col-md-3">
                
                <!--menu-->
                <div class="list-group list-group-flush">
                    <a v-for="t in tabs"
                       @click="tab=t"
                       :href="t.href"  
                       :class="{active: tab==t}" 
                       class="list-group-item list-group-item-action fw700 pt-3 pb-3"
                    >
                        <!--<i :class="t.icon" class="fa fa-fw"></i>-->
                        {{ t.title }}
                        <i class="float-right fa fa-angle-right"></i>
                    </a>
                </div>
                
            </div>
            
            <!--content-->
            <div v-if="tab" class="col-md-9">
                <component :is="tab.view"></component>
            </div>
        </div>
    `,

    watch: {
        tab: function(v) {
            Cookie.i.set('admin-view@tabIndex', `${this.tabs.indexOf(v)}`)
        }
    },

    data() {
        return {
            tabs: [
                {href: '#welcome', title: 'PÁGINA PRINCIPAL', icon: 'fa-home', view: AdminFrontPageView},
                {href: '#tutorials', title: 'TUTORIAIS', icon: 'fa-youtube-play', view: AdminTutorialsView},
                {href: '#blog', title: 'BLOG', icon: 'fa-comments', view: AdminBlogView},
                {href: '#ad', title: 'ANÚNCIOS', icon: 'fa-bullhorn', view: AdminAdView},
            ],
            tab: null,
        }
    },

    mounted() {
        this.tab = this.tabs[Cookie.i.get('admin-view@tabIndex', '0')];
    }

};

export default AdminView;