import rmodal from '../../components/modal/rmodal';
import AttachmentsView from "../attachment/attachments-view";
import Presenter from "../../presenters/presenter";

const _deepClone = require('lodash/cloneDeep');
const _mapValues = require('lodash/mapValues');


const AdminBlogPostDialog = {

    components: {rmodal, AttachmentsView},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text 
                ref="input-title" 
                v-model="model.title" field="title" ph="" label="Título"
                ></input-text>
                
        <input-text-area
                ref="input-body"
                v-model="model.body" field="body" label="Conteúdo" rows="4"
            ></input-text-area>
            
        
        <input-datetime 
            ref="input-published-at"
            v-model="model.published_at"
            field="published_at"
            label="Publicar em"
            ></input-datetime>

        <input-select
            ref="input-publisher-id"
            v-model="model.publisher_id"
            field="publisher_id"
            label="Publicador"
            :select-data="$publishersSelectData"
            ph="Escolha um..."
            ></input-select>
        
        <hr>
        
        <attachments-view 
            attachable-type="App\\Blog" 
            :attachable-id="model.id"
            :data="model.attachments"
            @on-added="onSavedAttachment"
            @on-destroyed="onDestroyedAttachment"
            ></attachments-view>
            
        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            publishers: []
        }
    },

    methods: {

        open(model)
        {
            this._pristineIt();

            // fixing published_at -> dd/mm/yyy format
            const clonedModel = _deepClone(model) || {id: 0, attachments: []};
            this.model = _mapValues(clonedModel, (v, k, o) => {
                if (k=='published_at' && v) return moment(o[k]).format('DD/MM/YYYY');
                return v;
            });

            this.$m.open().catch(()=>{});

            this.loadPublishers();
        },

        _pristineIt() {
            this.$pristineIt('input-title', 'input-body', 'input-published-at', 'input-publisher-id');
        },

        loadPublishers() {
            this.$get('admin/json/blog/publisher')
                .then(d => {
                    this.publishers = Presenter.blog(d);
                    this._pristineIt();
                });
        },

        submit() {
            const params = _deepClone(this.model);
            params.attachments = params.attachments.map(({id}) => id);

            this.$post('admin/json/blog', params)
                .then(d => {
                    this.$emit('on-saved', d);
                    this.$m.close();
                })
        },

        onSavedAttachment(model) {
            this.model.attachments.push(model);
        },

        onDestroyedAttachment(modelId) {
            this.model.attachments = this.model.attachments.filter(({id}) => modelId!=id)
        },

        cancel() {
            this.$m.close();
        },
    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        },
        $publishersSelectData() {
            return this.publishers.map(({id:key, name:desc}) => ({key, desc: `#${key} - ${desc}`}))
        }
    }
};

export default AdminBlogPostDialog;