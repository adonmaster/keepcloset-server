import AdminAdDialog from "./admin-ad-dialog";

const AdminAdView = {

    components: {AdminAdDialog},

    template: `
    <div class="p-2">
    
        <h2><i class="fa fa-angle-double-right"></i> ANÚNCIOS</h2>
        <hr>
        
        <!--error & loading-->
        <load-error :loading="loading" :error="error"></load-error>

        <!--list-->
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col" width="90">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Link</th>
                    <th width="120">
                        <button class="btn btn-success btn-sm" @click="edit(null, data.length)">
                            <i class="fa fa-plus fa-fw"></i> 
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, $index) in data" class="as-hover-opacity">
                    <th scope="row">
                        <button class="btn btn-primary btn-sm" @click="edit(item)">
                            <i class="fa fa-edit fa-fw"></i>
                            {{item.id}}
                        </button>
                    </th>
                    <td>{{item.body.title}}</td>
                    <td>{{item.body.desc}}</td>
                    <td>{{item.body.link}}</td>
                    <td>
                        <button class="btn btn-danger btn-sm" @click="destroy(item)">
                            <i class="fa fa-close"></i>
                        </button>                        
                    </td>
                </tr>
            </tbody>
        </table>
        
        <!--dialog-->
        <admin-ad-dialog 
            :parent-name="key_ads" 
            ref="edit-dialog"
            @on-saved="onSaved"
            ></admin-ad-dialog>
        
    </div>
    `,

    data() {
        return {
            loading: false,
            error: null,
            data: [],

            key_ads: 'ads'
        }
    },

    methods: {

        update() {
            this.$get('admin/json/generic/items', {parent: this.key_ads})
                .then(d => {
                    this.data = d;
                    console.log(d);
                });
        },

        edit(model, position) {
            this.$refs['edit-dialog'].open(model, position);
        },

        onSaved(model) {
            this.$listPush(this.data, model);
        },

        destroy(model) {
            this.$postDelete(`admin/json/generic/${model.id}/destroy`)
                .then(()=>{
                    this.data = this.data.filter(({id}) => id != model.id);
                });
        }

    },

    mounted() {
        this.update();
    }

};

export default AdminAdView;