import AdminBlogPublisher from "./admin-blog-publisher";
import AdminBlogPost from "./admin-blog-post";

const AdminBlogView = {

    name: 'AdminBlogView',

    components: {AdminBlogPublisher, AdminBlogPost},

    template: `
    <div class="p-2">
    
        <h2><i class="fa fa-angle-double-right"></i> BLOG</h2>
        <hr>
        
        <!--error & loading-->
        <load-error :loading="loading" :error="error"></load-error>

        <!--publishers-->
        <div class="pl-4">
            <admin-blog-publisher></admin-blog-publisher>
        </div>
        
        <!--blog-->
        <div class="pl-4 pt-5">
            <admin-blog-post></admin-blog-post>
        </div>
        
    </div>
    `,

    props: {},

    data() {
        return {
            loading: false,
            error: null,
        }
    },

    methods: {

    },

    mounted() {

    }

};

export default AdminBlogView;