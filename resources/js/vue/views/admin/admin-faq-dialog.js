import rmodal from '../../components/modal/rmodal';

const _deepClone = require('lodash/cloneDeep');

const AdminFaqDialog = {

    components: {rmodal},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text-area
                ref="input-question"
                v-model="body.question" field="question" label="Pergunta"
            ></input-text-area>
    
        <input-text-area
                ref="input-answer"
                v-model="body.answer" field="answer" label="Resposta"
            ></input-text-area>

        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    props: {
        parentName: {required: true}
    },

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            newImg: null,
            body: {}
        }
    },

    methods: {

        open(model, position)
        {
            this.newImg = null;
            this.model = _deepClone(model) || {id: 0, position};
            this.body = this.model.body || {};

            this.$pristineIt('input-question', 'input-answer');

            this.$m.open().catch(()=>{});
        },

        validate() {
            this.error = null;
            try {
                if (! this.body.question) throw Error('Pergunta não preenchida.');
                if (! this.body.answer) throw Error('Resposta não preenchida.');
                return true;
            }
            catch(err) {
                this.error = err.message;
            }
            return false;
        },

        submit() {
            if (this.validate()) {

                let params = {
                    'parent_name': this.parentName,
                    'id': this.model.id,
                    'body': this.body,
                    'position': this.model.position,
                };

                this.$post('admin/json/generics', params)
                    .then(d => {
                        this.$emit('on-saved', d);
                        this.$m.close();
                    });
            }
        },

        cancel() {
            this.$m.close();
        },
    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        }
    }

};

export default AdminFaqDialog;