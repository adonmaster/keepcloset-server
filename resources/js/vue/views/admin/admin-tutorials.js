import AdminTutorialsDialog from "./admin-tutorials-dialog";
import HashMap from "../../helpers/HashMap";
import Request from "../../helpers/Request";
import UndListControl from "../__list-control";

const _debounce = require('lodash/debounce');

const AdminTutorialsView = {

    name: 'AdminTutorialsView',

    components: {AdminTutorialsDialog, UndListControl},

    template: `
    <div class="p-2">
    
        <h2><i class="fa fa-angle-double-right"></i> TUTORIAIS</h2>
        
        <!--error & loading-->
        <load-error :loading="loading" :error="error"></load-error>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col" width="90">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Youtube</th>
                    <th width="120">
                        <button class="btn btn-success btn-sm" @click="edit(null, data.length)">
                            <i class="fa fa-plus fa-fw"></i> 
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, $index) in data" class="as-hover-opacity">
                    <th scope="row">
                        <button class="btn btn-primary btn-sm" @click="edit(item)">
                            <i class="fa fa-edit fa-fw"></i>
                            {{item.id}}
                        </button>
                    </th>
                    <td>{{item.body.title}}</td>
                    <td>{{item.body.desc}}</td>
                    <td>{{item.body.youtube}}</td>
                    <td>
                        <und-list-control class="as-hidden" 
                            :data="data" :index="$index"
                            @on-destroy="destroy"
                            @on-swapped="onSwapped"
                            ></und-list-control>                            
                    </td>
                </tr>
            </tbody>
        </table>
        
        <!--dialog-->
        <admin-tutorials-dialog 
            :parent-name="key_tutorials" 
            ref="edit-dialog"
            @on-saved="onSaved"
            ></admin-tutorials-dialog>
    </div>
    `,

    data() {
        return {
            key_tutorials: 'tutorials',
            loading: false,
            error: null,
            data: [],

            debouncePostPositions: _debounce(this._postPosition, 2100),
            hashPositions: new HashMap()
        }
    },

    methods: {

        update() {
            this.$get('admin/json/generic/items', {parent: this.key_tutorials})
                .then(d => {
                    this.data = d;
                });
        },

        edit(model, position) {
            this.$refs['edit-dialog'].open(model, position);
        },

        onSaved(model) {
            this.$listPush(this.data, model);
        },

        destroy(model) {
            this.$postDelete(`admin/json/generic/${model.id}/destroy`)
                .then(()=>{
                    this.data = this.data.map(parent => {
                        parent.items = parent.items.filter(({id}) => id != model.id);
                        return parent;
                    });
                });
        },

        /**
         * @param {SuSwapModel} swapObj
         */
        onSwapped(swapObj) {
            this.hashPositions.set(swapObj.a.id, swapObj.ia);
            this.hashPositions.set(swapObj.b.id, swapObj.ib);

            this.debouncePostPositions();
        },

        _postPosition() {
            const positions = this.hashPositions.toObject();
            this.hashPositions.clear();
            Request.post('admin/json/generic/positions', {positions}, {})
                .then(()=>{ })
        },

    },

    mounted() {
        this.update();
    }

};

export default AdminTutorialsView;