import AdminBlogPostDialog from "./admin-blog-post-dialog";
import Presenter from "../../presenters/presenter";

const AdminBlogPost = {

    components: {AdminBlogPostDialog},

    template: `
    <div>
        <h4><i class="fa fa-angle-right"></i> POSTS</h4>
        
        <!--error & loading-->
        <load-error :loading="loading" :error="error"></load-error>
        
        <!--tabela-->
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col" width="90">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Corpo</th>
                    <th scope="col">Publicador</th>
                    <th scope="col">Publicado para</th>
                    <th scope="col" width="30"></th>
                    <th width="120">
                        <button class="btn btn-success btn-sm" @click="edit(null)">
                            <i class="fa fa-plus fa-fw"></i> 
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, $index) in data" class="as-hover-opacity">
                    <th scope="row">
                        <button class="btn btn-primary btn-sm" @click="edit(item)">
                            <i class="fa fa-edit fa-fw"></i>
                            {{item.id}}
                        </button>
                    </th>
                    <td>{{item.title}}</td>
                    <td>{{item.p.body}}</td>
                    <td><i class="fa fa-user fa-fw"></i> {{item.publisher.name}}</td>
                    <td>
                        {{item.p.publishedAt}}
                        <span v-if="!item.p.isPublished" class="badge badge-danger">
                            Ainda não publicado
                        </span>
                    </td>
                    <td>
                        <span class="badge" :class="item.p.badgeAttachment">
                            <i class="fa fa-paperclip"></i>
                            {{item.attachments.length}}
                        </span>
                    </td>
                    <td>
                        <button @click="destroy(item)" class="btn btn-outline-danger btn-sm as-hidden">
                            <i class="fa fa-close"></i>
                        </button>                        
                    </td>
                </tr>
            </tbody>
        </table>
        
        <!--dialogs-->
        
        <admin-blog-post-dialog
            ref="edit-dialog"
            @on-saved="onSaved"
            ></admin-blog-post-dialog>
            
    </div>
    `,

    data() {
        return {
            loading: false,
            error: null,
            data: []
        }
    },

    methods: {

        update() {
            this.$get('admin/json/blog')
                .then(d => {
                    this.data = Presenter.blog(d)
                })
        },

        edit(model) {
            this.$refs['edit-dialog'].open(model);
        },

        onSaved(model) {
            this.$listPush(this.data, Presenter.blog(model));
        },

        destroy(model) {
            this.$postDelete(`admin/json/blog/${model.id}`)
                .then(()=>{
                    this.data = this.data.filter(({id}) => id!=model.id);
                })
        }

    },

    mounted() {
        this.update();
    }

};

export default AdminBlogPost;