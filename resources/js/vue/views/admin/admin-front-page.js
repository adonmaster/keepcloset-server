import AdminQmUsaDialog from "./admin-qm-usa-dialog";
import Presenter from "../../presenters/presenter";
import UndListControl from "../__list-control";
import HashMap from "../../helpers/HashMap";
import Request from "../../helpers/Request";
import AdminFaqDialog from "./admin-faq-dialog";
import AdminQmSomosDialog from "./admin-qm-somos-dialog";

const _debounce = require('lodash/debounce');

const AdminFrontPageView = {

    name: 'AdminFrontPageView',

    components: {AdminQmUsaDialog, AdminFaqDialog, AdminQmSomosDialog, UndListControl},

    // language=HTML
    template: `
    <div class="row">
        <div class="col-12">
            <!--error & loading-->
            <load-error :loading="loading" :error="error"></load-error>
            
            <!--quem usa-->
            <div class="p-2">
                <h2><i class="fa fa-angle-double-right"></i> QUEM USA</h2>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" width="90">#</th>
                            <th scope="col" width="35"><i class="fa fa-image"></i></th>
                            <th scope="col">Nome</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">insta</th>
                            <th width="120">
                                <button class="btn btn-success btn-sm" @click="edit('qm-usa-dialog', null, $dataQmUsa.length)">
                                    <i class="fa fa-plus fa-fw"></i> 
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, $index) in $dataQmUsa" class="as-hover-opacity">
                            <th scope="row">
                                <button class="btn btn-primary btn-sm" @click="edit('qm-usa-dialog', item)">
                                    <i class="fa fa-edit fa-fw"></i>
                                    {{item.id}}
                                </button>
                            </th>
                            <td><img :src="item.p.attachmentUrl" class="rounded-circle" width="30" height="30"></td>
                            <td>{{item.body.name}}</td>
                            <td>{{item.body.desc}}</td>
                            <td>{{item.body.insta}}</td>
                            <td>
                                <und-list-control class="as-hidden" 
                                    :data="$dataQmUsa" :index="$index"
                                    @on-destroy="destroy"
                                    @on-swapped="onSwapped"
                                    ></und-list-control>                            
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
                        
            <!--faq-->
            <div class="p-2">
                <h2><i class="fa fa-angle-double-right"></i> FAQ</h2>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" width="90">#</th>
                            <th scope="col" width="30%">Pergunta</th>
                            <th scope="col" >Resposta</th>
                            <th width="120">
                                <button class="btn btn-success btn-sm" @click="edit('faq-dialog', null, $dataFaq.length)">
                                    <i class="fa fa-plus fa-fw"></i>
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, $index) in $dataFaq" class="as-hover-opacity">
                            <th scope="row">
                                <button class="btn btn-primary btn-sm" @click="edit('faq-dialog', item)">
                                    <i class="fa fa-edit fa-fw"></i>
                                    {{item.id}}
                                </button>
                            </th>
                            <td>{{item.body.question}}</td>
                            <td>{{item.body.answer}}</td>
                            <td>
                                <und-list-control class="as-hidden" 
                                    :data="$dataFaq" :index="$index"
                                    @on-destroy="destroy"
                                    @on-swapped="onSwapped"
                                    ></und-list-control>                            
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
                        
            <!--quem somos-->
            <div class="p-2">
                <h2><i class="fa fa-angle-double-right"></i> QUEM SOMOS</h2>
                
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col" width="90">#</th>
                            <th scope="col" width="35"><i class="fa fa-image"></i></th>
                            <th scope="col">Nome</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">insta</th>
                            <th width="120">
                                <button class="btn btn-success btn-sm" @click="edit('qm-somos-dialog', null, $dataQmSomos.length)">
                                    <i class="fa fa-plus fa-fw"></i> 
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, $index) in $dataQmSomos" class="as-hover-opacity">
                            <th scope="row">
                                <button class="btn btn-primary btn-sm" @click="edit('qm-somos-dialog', item)">
                                    <i class="fa fa-edit fa-fw"></i>
                                    {{item.id}}
                                </button>
                            </th>
                            <td><img :src="item.p.attachmentUrl" class="rounded-circle" width="30" height="30"></td>
                            <td>{{item.body.name}}</td>
                            <td>{{item.body.desc}}</td>
                            <td>{{item.body.insta}}</td>
                            <td>
                                <und-list-control class="as-hidden" 
                                    :data="$dataQmSomos" :index="$index"
                                    @on-destroy="destroy"
                                    @on-swapped="onSwapped"
                                    ></und-list-control>                            
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            
            <!--dialogs-->
            
            <admin-qm-usa-dialog 
                :parent-name="key_welcome_qm_usa" 
                ref="qm-usa-dialog"
                @on-saved="onSavedQmUsa"
                ></admin-qm-usa-dialog>

            <admin-faq-dialog
                :parent-name="key_welcome_faq"
                ref="faq-dialog"
                @on-saved="onSavedFaq"
                ></admin-faq-dialog>

            <admin-qm-somos-dialog 
                :parent-name="key_welcome_qm_somos" 
                ref="qm-somos-dialog"
                @on-saved="onSavedQmSomos"
                ></admin-qm-somos-dialog>
                
        </div>
    </div>
    `,

    data() {
        return {
            key_welcome_qm_usa: 'welcome.qm-usa',
            key_welcome_faq: 'welcome.faq',
            key_welcome_qm_somos: 'welcome.qm-somos',

            loading: false,
            error: null,
            data: [],

            debouncePostPositions: _debounce(this._postPosition, 2100),
            hashPositions: new HashMap()
        }
    },

    methods: {

        update() {
            const parents = [
                this.key_welcome_qm_usa,
                this.key_welcome_faq,
                this.key_welcome_qm_somos,
            ];

            this.$get('admin/json/generics', {parents})
                .then(d => {
                    this.data = Presenter.generic(d);
                })
        },

        onSavedQmUsa(model) {
            this.$listPush(this.$dataQmUsa, Presenter.genericItem(model));
        },

        onSavedFaq(model) {
            this.$listPush(this.$dataFaq, Presenter.genericItem(model));
        },

        onSavedQmSomos(model) {
            this.$listPush(this.$dataQmSomos, Presenter.genericItem(model));
        },

        edit(refDialog, model, position) {
            this.$refs[refDialog].open(model, position);
        },

        destroy(model) {
            this.$postDelete(`admin/json/generic/${model.id}/destroy`)
                .then(()=>{
                    this.data = this.data.map(parent => {
                        parent.items = parent.items.filter(({id}) => id != model.id);
                        return parent;
                    });
                });
        },

        /**
         * @param {SuSwapModel} swapObj
         */
        onSwapped(swapObj) {
            this.hashPositions.set(swapObj.a.id, swapObj.ia);
            this.hashPositions.set(swapObj.b.id, swapObj.ib);

            this.debouncePostPositions();
        },

        _postPosition() {
            const positions = this.hashPositions.toObject();
            this.hashPositions.clear();
            Request.post('admin/json/generic/positions', {positions}, {})
                .then(()=>{ })
        },

        _initList(key) {
            let items = this.data.filter(({name}) => name===key);
            if (! items.length) {
                const newish = {name: key, items: []};
                this.data.push(newish);
                items.push(newish);
            }
            return items[0].items;
        }

    },

    computed: {

        $dataQmUsa() {
            return this._initList(this.key_welcome_qm_usa);
        },

        $dataFaq() {
            return this._initList(this.key_welcome_faq);
        },

        $dataQmSomos() {
            return this._initList(this.key_welcome_qm_somos);
        }

    },

    mounted() {
        this.update();
    }

};

export default AdminFrontPageView;