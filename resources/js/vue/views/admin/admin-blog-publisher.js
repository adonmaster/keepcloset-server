import AdminBlogPublisherDialog from "./admin-blog-publisher-dialog";
import ModalOk from '../../components/modal/rmodal-ok'

const AdminBlogPublisher = {

    components: {AdminBlogPublisherDialog, ModalOk},

    template: `
    <div>
        <h4><i class="fa fa-angle-right"></i> PUBLICADORES</h4>
        
        <!--error & loading-->
        <load-error :loading="loading" :error="error"></load-error>
        
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col" width="90">#</th>
                    <th scope="col" width="35"><i class="fa fa-image"></i></th>
                    <th scope="col">Nome</th>
                    <th scope="col">Insta</th>
                    <th width="120">
                        <button class="btn btn-success btn-sm" @click="edit(null, data.length)">
                            <i class="fa fa-plus fa-fw"></i> 
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, $index) in data" class="as-hover-opacity">
                    <th scope="row">
                        <button class="btn btn-primary btn-sm" @click="edit(item)">
                            <i class="fa fa-edit fa-fw"></i>
                            {{item.id}}
                        </button>
                    </th>
                    <td><img :src="item.avatar.url_thumb" class="rounded-circle" width="30" height="30"></td>
                    <td>{{item.name}}</td>
                    <td>{{item.insta}}</td>
                    <td>
                        <button @click="destroy(item)" class="btn btn-outline-danger btn-sm as-hidden">
                            <i class="fa fa-close"></i>
                        </button>                        
                    </td>
                </tr>
            </tbody>
        </table>
        
        <!--edit dialog-->
        <admin-blog-publisher-dialog 
            ref="edit-dialog"
            @on-saved="onSaved"
            ></admin-blog-publisher-dialog>
            
        <modal-ok ref="modal-ok">
            Tem certeza que deseja remover este registro?
        </modal-ok>                
    </div>
    `,

    data() {
        return {
            loading: false,
            error: null,
            data: [],
        }
    },

    methods: {

        update() {
            this.$get('admin/json/blog/publisher')
                .then(d => {
                    this.data = d;
                })
        },

        edit(model) {
            this.$refs['edit-dialog'].open(model);
        },

        onSaved(model) {
            this.$listPush(this.data, model, true);
        },

        destroy(model) {
            this.$refs['modal-ok'].open('Atenção')
                .then(()=> {

                    this.$postDelete(`admin/json/blog/publisher/${model.id}`)
                        .then(() => {
                            this.data = this.data.filter(({id}) => id!=model.id);
                        })

                });
        },

    },

    mounted() {
        this.update();
    }

};

export default AdminBlogPublisher;