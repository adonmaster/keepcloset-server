import rmodal from '../../components/modal/rmodal';


const _deepClone = require('lodash/cloneDeep');

const AdminQmSomosDialog = {

    components: {rmodal},

    template: `
    <rmodal ref="modal" has-footer="false" :title="title">
    
        <input-text 
                ref="input-name" 
                v-model="body.name" field="name" ph="" label="Nome"
                ></input-text>
    
        <input-text-area
                ref="input-desc"
                v-model="body.desc" field="desc" label="Descrição"
            ></input-text-area>
        
        <input-group 
                ref="input-insta" field="insta" label="Instagram"
                v-model="body.insta" ph="ex.: adonio.design">
            <div class="input-group-prepend" slot="prepend">
                <span class="input-group-text">
                    <i class="fa fa-instagram"></i>
                </span>
            </div>
        </input-group>
        
        <!--attachment img-->
        <input-file 
            ref="input-img"
            accept="image/*"
            field="img" v-model="newImg" label="Arquivo de imagem"
            ></input-file>
        
        <!--attachment saved    -->
        <div v-show="model.attachment">
            <div class="badge badge-primary">
                <i class="fa fa-file-o"></i>
                Arquivo já salvo no servidor, faça upload novamente para substituí-lo
            </div>
        </div>   

        <load-error :loading="loading" :error="error"></load-error>
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-default" @click="cancel">
                Cancelar
            </button>
            <button @click="submit" class="btn btn-success" >
                <i class="fa fa-check fa-fw"></i>
                Salvar
            </button>
        </div>
    </rmodal>
    `,

    props: {
        parentName: {required: true}
    },

    data() {
        return {
            loading: false,
            error: null,

            model: {},
            newImg: null,
            body: {}
        }
    },

    methods: {

        open(model, position)
        {
            this.newImg = null;
            this.model = _deepClone(model) || {id: 0, position};
            this.body = this.model.body || {};

            this.$pristineIt('input-name', 'input-desc', 'input-insta', 'input-img');

            this.$m.open().catch(()=>{});
        },

        validate() {
            this.error = null;
            try {
                if (! this.body.name) throw Error('Nome não preenchido.');
                if (! this.body.desc) throw Error('Descrição não preenchida.');
                if (! this.body.insta) throw Error('Instagram não preenchida.');
                if (!this.newImg && !this.model.attachment) throw Error('Arquivo não escolhido');
                return true;
            }
            catch(err) {
                this.error = err.message;
            }
            return false;
        },

        submit() {
            if (this.validate()) {

                let params = {
                    'parent_name': this.parentName,
                    'id': this.model.id,
                    'position': this.model.position,
                };

                // if there's file, the request will be a multipart
                if (this.newImg) {
                    params['body[name]'] = this.body.name;
                    params['body[desc]'] = this.body.desc;
                    params['body[insta]'] = this.body.insta;
                    params['file'] = this.newImg;
                }
                // otherwise, just a json
                else {
                    params['body'] = this.body;
                }

                this.$post('admin/json/generics', params)
                    .then(d => {
                        this.$emit('on-saved', d);
                        this.$m.close();
                    });
            }
        },

        cancel() {
            this.$m.close();
        },
    },

    computed: {
        $m() {
            return this.$refs['modal'];
        },
        title() {
            return this.model && this.model.id
                ? `Editando #${this.model.id} ...`
                : 'Novo ...';
        }
    }

};

export default AdminQmSomosDialog;