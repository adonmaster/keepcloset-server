import AttachmentPreviewDialog from "./attachment-preview-dialog";

const AttachmentsView = {

    components: {AttachmentPreviewDialog},

    // language=HTML
    template: `
    <div class="pl-2 pr-2 pb-2 mdc-bg-green-50">
    
        <!--edit mode-->
        <div v-if="mode=='edit'">
            <small>Anexos:</small>
            <div>
                
                <button @click="showAdd=!showAdd" class="btn btn-outline-primary btn-sm">
                    <i class="fa fa-paperclip"></i>
                    <i class="fa fa-plus"></i>
                </button>     
                
                <button
                    v-for="item in data" 
                    @click="attachmentPreview(item)" 
                    class="btn btn-sm ml-2"
                    :class="{'btn-danger': item.type=='youtube', 'btn-light': item.type=='image'}">
                    
                    #{{item.id}}
                    <img v-if="item.type=='image'"
                        :src="item.url" 
                        width="24px" 
                        height="24px" 
                        class="rounded-circle" 
                        style="object-fit: cover">
                        
                    <i v-if="item.type=='youtube'" class="fa fa-youtube-play"></i>
                </button>
                
            </div>
            
            <!--panel add-->
            <transition-fade in="fadeIn">
            <div v-if="showAdd">
                <load-error :loading="loading" :error="error"></load-error>
    
                <hr>
                <input-radio 
                    ref="input-type"
                    field="type"
                    v-model="type" 
                    :radio-data="$typeData"
                    ></input-radio>
                    
                <!--youtube-->
                <div v-if="type=='youtube'" id="youtube">
                
                    <input-group 
                        ref="input-youtube" field="url" label="Youtube Link"
                        v-model="url" ph="">
                        
                        <div class="input-group-prepend" slot="prepend">
                            <span class="input-group-text">
                                <i class="fa fa-youtube-play"></i>
                            </span>
                        </div>
                    </input-group>
                </div>
                
                <!--image-->
                <div v-if="type=='image'" id="image">
                    <input-file 
                        ref="input-file"
                        accept="image/*"
                        field="file" v-model="file" label="Arquivo de imagem"
                        ></input-file>
                </div>
    
                <div class="text-right">
                    <button @click="reset" class="btn btn-light btn-sm">
                        <i class="fa fa-close fa-fw"></i> Cancelar
                    </button>                
                    <button @click="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-check fa-fw"></i> Adicionar anexo
                    </button>
                </div>
                
            </div>
            </transition-fade>
            <!--panel add-->
        </div>
        
        <!--preview mode-->
        <div v-if="mode=='preview'" class="pt-2 pb-2">
            
            <!--header-->
            <h4>
                <button @click="mode='edit'" class="btn btn-outline-secondary btn-sm">
                    <i class="fa fa-arrow-left"></i>
                </button>
                Anexo #{{model.id}}
            </h4>
            
            <load-error :loading="loading" :error="error"></load-error>

            <!--youtube-->
            <div v-if="model.type=='youtube'" class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BCnydqH0s64" allowfullscreen></iframe>
            </div>
            
            <!--image file-->
            <img 
                v-if="model.type=='image'"
                :src="model.url" width="100%" style="object-fit: cover">
            
            <!--footer-->
            <div id="actions" class="text-center">
                <hr>
                <button @click="attachmentDestroy(model)" class="btn btn-danger btn-sm">
                    <i class="fa fa-trash fa-fw"></i>
                    APAGAR ANEXO
                </button>
            </div>
        </div>

    </div>
    `,

    props: {
        data: {default: ()=>[]},
        attachableType: {required: true},
        attachableId: {required: true},
    },

    data() {
        return {
            loading: false,
            error: null,

            showAdd: false,
            mode: 'edit',
            model: null,

            type: 'youtube',
            url: null,
            file: null
        }
    },

    methods: {

        reset() {
            this.$pristineIt('input-youtube', 'input-file');
            this.type = 'youtube';
            this.showAdd = false;
            this.mode = 'edit';
            this.url = null;
            this.file = null;
        },

        submit() {
            let params = {
                'attachable_type': this.attachableType,
                'attachable_id': this.attachableId,
                'type': this.type
            };

            if (this.type=='image') {
                params['file'] = this.file
            }
            else if (this.type=='youtube') {
                params['url'] = this.url
            }

            this.$post('admin/json/attachment/create', params)
                .then(d => {
                    this.$emit('on-added', d);
                    this.reset();
                })
        },

        attachmentPreview(model) {
            this.model = model;
            this.mode = 'preview';
        },

        attachmentDestroy(model) {
            this.$postDelete(`admin/json/attachment/${model.id}`)
                .then(()=> {
                    this.$emit('on-destroyed', model.id);
                    this.mode = 'edit';
                })
        }

    },

    computed: {
        $typeData() {
            return [
                {value: 'youtube', body: `<i class="fa fa-youtube fa-fw"></i> Youtube`},
                {value: 'image', body: `<i class="fa fa-file-image-o fa-fw"></i> Imagem`}
            ]
        }
    },

    mounted() {
        this.reset()
    }


};

export default AttachmentsView;