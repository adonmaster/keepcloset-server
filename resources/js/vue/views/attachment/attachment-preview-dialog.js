import rmodal from '../../components/modal/rmodal';

const AttachmentPreviewDialog = {

    components: {rmodal},

    template: `
    <rmodal ref="modal" has-footer="false" :title="'Anexo #'+model.id">
    
        <load-error :loading="loading" :error="error"></load-error>

        <!--youtube-->
        <div v-if="model.type=='youtube'" class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BCnydqH0s64" allowfullscreen></iframe>
        </div>
        
        <!--image file-->
        <img 
            v-if="model.type=='image'"
            :src="model.url" width="100%" style="object-fit: cover">
        
        <div slot="footer" class="modal-footer" >
            <hr style="margin: 0 0 14px 0;">

            <button class="btn btn-danger" @click="destroy">
                <i class="fa fa-trash"></i>
                Apagar anexo
            </button>
            <button @click="ok" class="btn btn-light" >
                <i class="fa fa-check fa-fw"></i>
                OK
            </button>
        </div>
    </rmodal>
    `,

    data() {
        return {
            loading: false,
            error: null,
            model: {}
        }
    },

    methods: {

        open(model) {
            this.model = model;
            this.$m.open().catch(()=>{});
        },

        ok() {
            this.$m.close();
        },

        destroy() {
            this.$m.close();
        }

    },

    computed: {
        $m() {
            return this.$refs['modal'];
        }
    }

};

export default AttachmentPreviewDialog;