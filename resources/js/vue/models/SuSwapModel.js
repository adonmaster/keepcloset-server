/**
 * @property ia
 * @property a
 * @property ib
 * @property b
 */
export default class SuSwapModel {

    constructor(ia, a, ib, b) {
        this.ia = ia;
        this.a = a;
        this.ib = ib;
        this.b = b;
    }

}