const UtilMixin = {

    methods: {

        $listPush(list, model, insertAtBeginning=false) {
            const index = list.findIndex(({id}) => model.id == id);
            if (~index) {
                this.$set(list, index, model)
            } else {
                if (insertAtBeginning) {
                    list.unshift(model)
                } else {
                    list.push(model);
                }
            }
        },

        $dataPush(model, insertAtBeginning=false, paginated=true) {
            if (this.data) {
                let l = paginated ? this.data.data : this.data;
                let index = l.findIndex(({id}) => model.id == id);
                if (~index) {
                    this.$set(l, index, model);
                } else {
                    if (insertAtBeginning) {
                        l.unshift(model)
                    } else {
                        l.push(model);
                    }
                }
            } else {
                if (paginated) {
                    this.data = {data: [model]}
                } else {
                    this.data = [model];
                }
            }
        },

        $pristineIt(... references) {
            references.forEach(r => {
                let component = this.$refs[r];
                if (component) component.pristineIt();
            })
        }

    }

};

export default UtilMixin;