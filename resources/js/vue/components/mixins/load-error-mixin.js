const LoadErrorMixin = {

    data() {
        return {
            loading: false,
            error: null
        }
    }

};

export default LoadErrorMixin;