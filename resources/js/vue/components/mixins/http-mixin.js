import Request from '../../helpers/Request'
import Url from "../../helpers/Url";


const HttpMixin = {

    methods: {

        $url(uri) {
            return Url.i.to(uri);
        },

        $serializeFrom(event) {
            return this.$serializeFromForm(event.target);
        },

        $serializeFromForm(form) {
            return $(form)
                .serializeArray()
                .reduce((ac, cur) => {
                    ac[cur.name] = cur.value;
                    return ac;
                }, {});
        },

        /**
         * @param {string} uri
         * @param {Object} paramData
         * @returns {Promise}
         */
        $post(uri, paramData) {
            return Request.post(uri, paramData, this)
        },

        /**
         * @param uri
         * @param paramData
         * @returns {Promise}
         */
        $postDelete(uri, paramData) {
            return Request.delete(uri, paramData, this)
        },

        /**
         *
         * @param {string} uri
         * @param {object} params
         * @returns {Promise}
         */
        $get(uri, params={}) {
            return Request.get(uri, this, params)
        }
    }

};

export default HttpMixin;