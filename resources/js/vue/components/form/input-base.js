import Request from '../../helpers/Request'

const _get = require('lodash/get');

const InputBase = {

    props: {
        value: {},
        field: {required: true},
        inputType: {default: null},
        label: {default: null},

        rows: {default: 3},

        inputGroupClass: null,
        inputClass: {type: String},
        labelClass: {type: String},
        invalidClass: null,
        ph: null,
        ac: null,

        selectData: {type: Array, default: ()=>[]},
        radioData: {type: Array, default: ()=>[]},
    },

    data() {
        return {
            pristine: true,
            error: null,
            keyReqListener: 'null'
        }
    },

    methods: {

        pristineIt() {
            this.pristine = true;
            this.error = null;
        },

        onValidate(data) {
            this.pristine = false;
            this.error = _get(data, `errors.${this.field}.0`, null);
        }

    },

    computed: {
        $type() {
            return this.inputType===null ? 'text' : this.inputType;
        },
        $inputClass() {
            let r = (this.inputClass || '');
            if ( ! this.pristine) {
                r += this.error ? ' is-invalid' : ' is-valid';
            }
            return r;
        }
    },

    mounted() {
        this.keyReqListener = Request.registerValidatorListener(this.onValidate);
    },

    beforeDestroy() {
        Request.unregisterValidatorListener(this.keyReqListener);
    }

};

export default InputBase;