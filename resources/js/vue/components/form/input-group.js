import InputBase from './input-base'

const InputGroup = {

    extends: InputBase,

    template: `
    <div class="form-group">
        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
        <div class="input-group" :class="inputGroupClass">
        
            <slot name="prepend"></slot>
            
            <input 
                :type="$type" 
                :name="field" 
                :id="field" 
                class="form-control" 
                :class="$inputClass" 
                :placeholder="ph"
                :autocomplete="ac"
                
                :value="value"
                @input="$emit('input', $event.target.value)"
                >
                
            <slot name="append"></slot>
            
            <div class="invalid-feedback" :class="invalidClass">
                {{ error }}
            </div>
        </div>
        
    </div>
    `,

};

export default InputGroup;