import InputBase from "./input-base";
import Task from "../../helpers/Task";

const InputSelect = {

    extends: InputBase,

    template: `
    <div class="form-group">

        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>

        <select
            ref="select" 
            class="form-control" :id="field" :name="field"
            :class="$inputClass" 
            :value="value" @input="$emit('input', $event.target.value)"
            >
            <option v-if="ph" disabled value="">{{ ph }}</option>
            <option v-for="i in selectData" :value="i.key">{{ i.desc }}</option>
        </select>
        
        <div class="invalid-feedback" :class="invalidClass">
            {{ error }}
        </div>
    </div>
    `,

    watch: {
        selectData: function(v) {
            Task.i.wait(0).then(()=>{
                this.$emit('input', this.value || null)
            })
        }
    }

};

export default InputSelect;