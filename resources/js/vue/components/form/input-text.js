import InputBase from './input-base'

const InputText = {

    extends: InputBase,

    template: `
    <div class="form-group">
        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
        <input 
            :type="$type" 
            :name="field"
            :id="field" 
            class="form-control" 
            :class="$inputClass" 
            :placeholder="ph"
            :autocomplete="ac"
            
            :value="value"
            @input="$emit('input', $event.target.value)"
            >

            <div class="invalid-feedback" :class="invalidClass">
                {{ error }}
            </div>
        </div>
    </div>
    `,

};

export default InputText;