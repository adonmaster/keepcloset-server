import InputBase from "./input-base";

const InputRadio = {

    extends: InputBase,

    template: `
    <div class="form-group">
        <div class="row mb-4">
            <div class="col-12">
                <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
            </div>
            
            <div v-for="i in radioData" class="col-6">
            
                <div class="form-check text-center">
                    <input class="form-check-input" type="radio" 
                        :name="field" :id="i.value" v-model="value"
                        :value="i.value" @input="$emit('input', $event.target.value)"
                        >
                    <label class="form-check-label" :for="i.value"
                        :class="{'mdc-text-blue': value==i.value}" v-html="i.body"
                        >
                    </label>
                </div>
                
            </div>
            
        </div>
        <div class="invalid-feedback" :class="invalidClass">
            {{ error }}
        </div>
    </div>
    `,

};

export default InputRadio;
