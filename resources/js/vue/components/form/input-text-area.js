import InputBase from './input-base'

const InputTextArea = {

    extends: InputBase,

    template: `
    <div class="form-group">
        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
        <textarea 
            class="form-control" :class="$inputClass" :id="field" :name="field" :rows="rows"
            :placeholder="ph" :value="value" @input="$emit('input', $event.target.value)"
            ></textarea>
            
        <div class="invalid-feedback" :class="invalidClass">
            {{ error }}
        </div>
    </div>
    `,

};

export default InputTextArea;