import InputBase from './input-base'

const _get = require('lodash/get');

const InputFile = {

    extends: InputBase,

    template: `
    <div class="form-group">
    
        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
        
        <div class="custom-file">
            <input 
                ref="input"
                type="file" :class="$inputClass" class="custom-file-input" :id="field" :name="field"
                @change="onChange" :accept="accept"
                >
            <label class="custom-file-label" :for="field">
                {{ desc || ph || 'Escolher arquivo...' }}
            </label>
            
            <div class="invalid-feedback" :class="invalidClass">
                {{ error }}
            </div>
        </div>
    </div>
    `,

    props: {
        accept: {default: '.pdf'}
    },

    data() {
        return {
            desc: null
        }
    },

    methods: {

        onChange(e) {
            let files = e.target.files;
            this.$emit('input', files);

            this.desc = _get(files, '0.name');
        },

        pristineIt() {
            this.$refs['input'].value = null;
            this.pristine = true;
            this.error = null;
            this.desc = null;
        }

    },

};

export default InputFile;