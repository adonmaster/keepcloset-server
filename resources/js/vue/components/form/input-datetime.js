import InputBase from "./input-base";
import Str from "../../helpers/Str";

/**
 * @link https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js
 * @link https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css
 * @link https://tempusdominus.github.io/bootstrap-4/Usage/
 *
 * @type {{template: string}}
 */
const InputDatetime = {

    name: 'InputDatetime',

    extends: InputBase,

    template: `
    <div class="form-group">
    
        <label v-if="label" :for="field" v-text="label" :class="labelClass"></label>
        
        <div class="input-group date" :id="'datetime_'+$uiid" data-target-input="nearest">
            
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
        
            <input ref="input" type="text" :id="field" :name="field" class="form-control datetimepicker-input" 
                data-toggle="datetimepicker" :data-target="'#datetime_'+$uiid"
                :placeholder="ph" autocomplete="off" 
                :value="value" @input="$emit('input', $event.target.value)"
                :class="$inputClass"
                />
                
            <div class="input-group-append">
                <button class="btn btn-secondary input-group-text" @click="clear">
                    <i class="fa fa-close"></i>
                </button>
            </div>
            
            <div class="invalid-feedback" :class="invalidClass">
                {{ error }}
            </div>
        </div>
        
    </div>
    `,

    computed: {
        $uiid() {
            return Str.random(16)
        }
    },

    methods: {

        clear() {
            this.$emit('input', '');
            this.$refs['input'].value = '';
        }

    },

    mounted() {
        const selector = '#datetime_'+this.$uiid;

        const options = {
            locale: 'pt-br',
            format: 'L',
            buttons: {
                showToday: true,
                showClear: true,
            },
            // keepInvalid: true
        };

        const vm = this;
        $(selector).datetimepicker(options);
        $(selector).on('change.datetimepicker', ({date}) => {
                vm.$emit('input', date.format('L'));
            });
    }

};

export default InputDatetime;