import Velocity from 'velocity-animate';
import Dom from "../../helpers/Dom";

let TransitionListFade = {

    template: `
        <transition-group
            name="staggered-fade"
            :tag="tag"
            :css="false"
            @before-enter="beforeEnter"
            @enter="enter"
            @leave="leave"
          >
              <slot></slot>
          </transition-group>
    `,

    props: {tag: {default: 'div'}, staggerEnter: {default: 150}, staggerLeave: {default: 100}},

    methods: {

        beforeEnter: function (el) {
            el.style.opacity = 0;
        },

        enter: function (el, done)
        {
            if (this.staggerEnter == 0) return done();

            const delay = Dom.findIndex(el) * this.staggerEnter;
            setTimeout(function () {
                Velocity(
                    el,
                    { opacity: 1 },
                    { complete: done }
                )
            }, delay)
        },

        leave: function (el, done) {
            if (this.staggerLeave == 0) return done();

            const delay = Dom.findIndex(el) * this.staggerLeave;
            setTimeout(function () {
                Velocity(
                    el,
                    { opacity: 0 },
                    { complete: done }
                )
            }, delay)
        }

    }

};

export default TransitionListFade;