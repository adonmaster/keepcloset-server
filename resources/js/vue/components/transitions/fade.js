let Fade = {
    template: `
        <transition name="custom-classes-transition"
                        :enter-active-class="enterClass"
                        :leave-active-class="leaveClass"
                        :mode="mode"
                        @before-enter="enter"
                        @leave="leave">
            <slot></slot>
        </transition>
    `,

    props: {
        in: {default: 'fadeInDown'},
        out: {default: 'fadeOutDown'},
        mode: {default: 'in-out'}
    },

    methods: {

        enter(el, done) {
            $(el).slideDown({complete: done});
        },

        leave(el, done) {
            $(el).slideUp({complete: done});
        }

    },

    computed: {
        enterClass() {
            return 'animated ' + this.in
        },
        leaveClass() {
            return 'animated ' + this.out
        }
    }
};

export default Fade;