const AlertSuccess = {

    template: `
    <div>
    
        <transition-fade in="fadeInUp" out="fadeOutDown">
        <div v-if="messageDesc" class="text-center">
            <div class="alert alert-success" style="word-break: break-all;">
                <i class="fa fa-check fa-3x"></i> <br>
                <div v-html="messageDesc"></div>
            </div>
        </div>
        </transition-fade>
        
    </div>
    `,

    props: {
        message: {default: null}
    },

    data() {
        return {
            messageDesc: null
        }
    },

    methods: {

        show(message) {
            this.messageDesc = message
        }

    },

    mounted() {
        const vm = this;

        this.$watch('message', function(v) {
            vm.messageDesc = v;
        }, {immediate: true})
    }

};

export default AlertSuccess;