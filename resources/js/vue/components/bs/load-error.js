const LoadError = {

    template: `
    <div>
        <!--error-->
        <transition-fade in="fadeInUp" out="fadeOutDown">
            <div v-if="error" class="text-center">
                <div class="alert alert-danger" style="word-break: break-all;">
                    <i class="fa fa-close fa-3x"></i> <br>
                    {{ error }}
                </div>
            </div>
        </transition-fade>
        
        <!--progress bar-->
        <transition-fade in="fadeInUp" out="fadeOutDown">
            <div v-if="loading" class="progress mt-1" >
                <div class="progress-bar progress-bar-striped progress-bar-animated" style="width: 100%"></div>
            </div>
        </transition-fade>
    </div>
    `,

    props: {
        loading: {required: true},
        error: {required: true}
    }

};

export default LoadError;