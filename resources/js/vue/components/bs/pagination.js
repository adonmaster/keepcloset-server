const Pagination = {

    template: `
    <nav v-show="$hasPrev || $hasNext">
        <ul class="pagination justify-content-center">
            <li class="page-item" :class="{disabled: !$hasPrev}">
                <a v-if="$hasPrev" class="page-link" href="#" @click.prevent="onPrev">
                    <i class="fa fa-angle-left"></i>                
                </a>
                <span v-else class="page-link">
                    <i class="fa fa-angle-left"></i>
                </span>
            </li>
            
            <li class="page-item" :class="{disabled: !$hasNext}">
                <a v-if="$hasNext" class="page-link" href="#" @click.prevent="onNext">
                    <i class="fa fa-angle-right"></i>                
                </a>
                <span v-else class="page-link">
                    <i class="fa fa-angle-right"></i>
                </span>
            </li>

        </ul>
    </nav>
    `,

    props: {
        data: {default: ()=>{}}
    },

    methods: {

        onPrev() {
            this.$emit('on-prev', this.data.prev_page_url)
        },

        onNext() {
            this.$emit('on-next', this.data.next_page_url)
        }

    },

    computed: {

        $hasPrev() {
            return this.data && this.data.prev_page_url
        },

        $hasNext() {
            return this.data && this.data.next_page_url
        }

    },

    mounted() {
        //
    }

};

export default Pagination;