import Task from "../../helpers/Task";

const Flash = {

    bind(el, binding, vnode) {
        let $el = $(el);
        let value = binding.value;
        $el.data('v-flash', ()=>{
            $el.addClass(value);
            Task.i.wait(300)
                .then(() => {
                    $el.removeClass(value)
                })
        });
    }

};

export default Flash;