const BackgroundImageDirective = {

    bind(el, binding, vnode) {
        let $el = $(el);
        let value = binding.value;
        $el.css('background-image', `url('${value}')`);

        let keys = Object.keys(binding.modifiers);
        if (keys.length) $el.css('background-size', keys[0]);
    }

};

export default BackgroundImageDirective;