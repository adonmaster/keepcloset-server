/**
 * https://github.com/voronianski/jquery.avgrund.js
 * npm install rmodal --save
 */
import RModal from 'rmodal';

export default {

    //language=HTML
    template: `
    <div id="modal" class="modal" style="z-index: 9999;">
        <div class="modal-dialog animated-short" :style="{width: finalWidth}"> <!-- width -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 ref="title" v-text="title"></h4>
                </div>

                <div class="modal-body">
                    <slot></slot>
                </div>
                
                <slot name="footer"></slot>
                
                <div v-if="hasFooter!='false'" class="modal-footer">
                    <hr style="margin: 0 0 14px 0;">
                    <button class="btn btn-default" @click.prevent="close(false)" v-text="textCancel">
                    </button>
                    <button class="btn" :class="classOk" @click.prevent="close(true)">
                        <i class="fa fa-fw" :class="iconOk"></i>
                        {{textOk}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    `,

    props: {
        width: {default: '600px'},
        hasFooter: {default: 'true'},
        title: {default: 'Atenção!'},

        textOk: {default: 'OK'},
        textCancel: {default: 'CANCELAR'},

        classOk: {default: 'btn-primary'},
        iconOk: {default: 'fa-check'}
    },

    data() {
        return {
            modal: null,
            resolved: false
        }
    },

    methods: {

        open() {
            let vm = this;
            return new Promise(function(resolve, reject) {
                vm.modal = new RModal(vm.$el, {
                    dialogOpenClass: 'fadeInUp',
                    dialogCloseClass: 'fadeOutDown',
                    afterClose() {
                        vm.resolved ? resolve() : reject();
                    },
                    escapeClose: true
                });
                vm.modal.open();
            });
        },

        close(resolved) {
            this.resolved = resolved;
            if (this.modal) this.modal.close();
        }

    },

    computed: {

        finalWidth() {
            let w = parseInt(this.width);
            return Math.min($(document).width(), w) + 'px';
        }

    },

    mounted() {
    }

}