import rmodal from './rmodal'

const RModalOk = {

    components: {rmodal},

    template: `
        <rmodal ref="modal" :title="$title" has-footer="true" :text-ok="textOk" :text-cancel="textCancel"
            :class-ok="classOk" :icon-ok="iconOk"
        >
            <slot></slot>
        </rmodal>
    `,

    props: {
        textOk: {default: 'OK'},
        textCancel: {default: 'CANCELAR'},
        classOk: {default: 'btn-primary'},
        iconOk: {default: 'fa-check'},
        title: {default: 'Atenção'}
    },

    data() {
        return {
            mTitle: null
        }
    },

    methods: {

        open(title=null) {
            this.mTitle = title;
            return this.$refs['modal'].open();
        }

    },

    computed: {
        $title() {
            return this.mTitle || this.title
        }
    }

};

export default RModalOk;